<?php

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Kodix\Payments\Models\AccountModel;
use Kodix\Payments\Models\CurrencyModel;

define('ADMIN_MODULE_NAME', 'kodix.payments');

Loc::loadLanguageFile(__DIR__ . '/install.php');
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/options.php");

$MODULE_RIGHT = $APPLICATION->GetGroupRight(ADMIN_MODULE_NAME);
if ($MODULE_RIGHT != 'W') {
    $APPLICATION->AuthForm(GetMessage('ACCESS_DENIED'));
}

if (!Loader::includeModule(ADMIN_MODULE_NAME)) {
    ShowError(GetMessage('KODIX_PAYMENTS_OPTIONS_ERR_MODULE_NOT_INSTALLED'));
} else {

    $publicKey = COption::GetOptionString(ADMIN_MODULE_NAME, 'public_key');
    $privateKey = COption::GetOptionString(ADMIN_MODULE_NAME, 'private_key');
    $folder = COption::GetOptionString(ADMIN_MODULE_NAME, 'folder', 'payments');
    $currency = COption::GetOptionString(ADMIN_MODULE_NAME, 'currency', 'RUB');

    $currencies = [];
    foreach (CurrencyModel::getList() as $entity) {
        $currencies[$entity->id] = $entity->name;
    }

    if (!strlen($privateKey) || !strlen($publicKey)) {
        list($publicKey, $privateKey) = \Kodix\Payments\Models\AccountModel::getRSAKeys();
    }

    $arResult = [
        'folder' => !is_null($_POST['folder']) ? $_POST['folder'] : $folder,
        'public_key' => !is_null($_POST['public_key']) ? $_POST['public_key'] : $publicKey,
        'private_key' => !is_null($_POST['private_key']) ? $_POST['private_key'] : $privateKey,
        'currency' => !is_null($_POST['currency']) ? $_POST['currency'] : $currency,
    ];

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        if (!AccountModel::validateRSAKeys($arResult['public_key'], $arResult['private_key'])) {
            $arErrors[] = GetMessage('KODIX_PAYMENTS_OPTIONS_ERR_KEYS');
        }

        if (empty($arErrors)) {
            if ($folder != $arResult['folder']) {
                COption::SetOptionString(ADMIN_MODULE_NAME, 'folder', trim($arResult['folder'], DIRECTORY_SEPARATOR));
            }
            if ($privateKey != $arResult['private_key']) {
                COption::SetOptionString(ADMIN_MODULE_NAME, 'private_key', $arResult['private_key']);
            }
            if ($publicKey != $arResult['public_key']) {
                COption::SetOptionString(ADMIN_MODULE_NAME, 'public_key', $arResult['public_key']);
            }
            if ($currency != $arResult['currency']) {
                COption::SetOptionString(ADMIN_MODULE_NAME, 'currency', $arResult['currency']);
            }
            LocalRedirect($_SERVER['REQUEST_URI']);
        }
    }


    if (!empty($arErrors)) {
        $message = new CAdminMessage(implode("<br>", $arErrors));
        echo $message->Show();
    }
    $arTabs = [
        [
            'DIV' => 'folder',
            'TAB' => GetMessage('KODIX_PAYMENTS_OPTIONS_COMMON'),
            'TITLE' => GetMessage('KODIX_PAYMENTS_OPTIONS_COMMON_TITLE'),
        ],
        [
            'DIV' => 'auth',
            'TAB' => GetMessage('KODIX_PAYMENTS_OPTIONS_KEYS'),
            'TITLE' => GetMessage('KODIX_PAYMENTS_OPTIONS_KEYS_TITLE'),
        ],
    ];
    $form = new CAdminForm('folder_form', $arTabs);
    $form->Begin(['FORM_ACTION' => sprintf(
        '%s/mid=%s&lang=%s&mid_menu=1',
        $APPLICATION->GetCurPage(),
        MODULE_ID
    )]);
    $form->BeginNextFormTab();
    $form->AddEditField(
        'folder',
        GetMessage('KODIX_PAYMENTS_OPTIONS_FOLDER_PATH'),
        true,
        ['size' => 40, 'maxlength' => 255],
        htmlspecialchars($arResult['folder'])
    );
    $form->AddDropDownField(
        'currency',
        GetMessage('KODIX_PAYMENTS_OPTIONS_DEFAULT_CURRENCY'),
        true,
        $currencies,
        $arResult['currency']
    );
    $form->BeginNextFormTab();
    $form->AddTextField(
        'public_key',
        GetMessage('KODIX_PAYMENTS_OPTIONS_KEYS_PUBLIC_KEY'),
        htmlspecialchars($arResult['public_key']),
        ['cols' => 75, 'rows' => 10],
        true
    );
    $form->AddTextField(
        'private_key',
        GetMessage('KODIX_PAYMENTS_OPTIONS_KEYS_PRIVATE_KEY'),
        htmlspecialchars($arResult['private_key']),
        ['cols' => 75, 'rows' => 29],
        true
    );
    $form->Buttons(['btnSave' => false]);
    $form->Show();
}