<?php

namespace Kodix\Payments;

use Bitrix\Main\ObjectPropertyException;

trait Accessor
{
    /** @inheritdoc */
    public function __get($name)
    {
        if (property_exists($this, $name)) {
            $accessorName = 'get' . ucfirst($name);
            if (method_exists($this, $accessorName)) {
                return $this->$accessorName();
            }
        }
        throw new ObjectPropertyException($name);
    }

    /** @inheritdoc */
    public function __set($name, $value)
    {
        if (property_exists($this, $name)) {
            $accessorName = 'set' . ucfirst($name);
            if (method_exists($this, $accessorName)) {
                $this->$accessorName($value);
                return;
            }
        }
        throw new ObjectPropertyException($name);
    }
}