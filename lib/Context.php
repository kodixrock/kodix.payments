<?php

namespace Kodix\Payments;

use Bitrix\Main\Application;
use Kodix\Payments\JWT\JWT;
use Kodix\Payments\Models\AccountModel;

class Context
{
    use Accessor;

    /**
     * @var Context[]
     */
    private static $instances = [];

    /**
     * @return Context
     */
    public static function getInstance()
    {
        $class = static::class;
        if (!array_key_exists($class, self::$instances)) {
            self::$instances[$class] = new static();
        }
        return self::$instances[$class];
    }

    /**
     * @var \Bitrix\Main\HttpRequest
     */
    private $request;

    /**
     * @var \Bitrix\Main\HttpResponse
     */
    private $response;

    /**
     * @var AccountModel
     */
    private $account;

    protected function __construct()
    {
        $context = Application::getInstance()->getContext();
        $this->request = $context->getRequest();
        $this->response = $context->getResponse();
    }

    /**
     * @return \Bitrix\Main\HttpRequest
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @return \Bitrix\Main\HttpResponse
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @return AccountModel
     */
    public function getAccount()
    {
        if (is_null($this->account)) {
            $type = null;
            $token = null;
            foreach (getallheaders() as $name => $value) {
                if (strtolower($name) == 'authorization') {
                    list($type, $token) = explode(' ', $value, 2);
                    break;
                }
            }

            if (strtolower($type) == 'bearer') {
                try {
                    $key = \COption::GetOptionString('kodix.payments', 'public_key');
                    $payload = JWT::decode($token, $key, ['RS256']);
                    $account = AccountModel::getById($payload->sub);
                    if (!is_null($account) && $account->active) {
                        $this->account = $account;
                    }
                } catch (\Exception $e) {
                    $this->account = null;
                }
            }
        }
        return $this->account;
    }


}