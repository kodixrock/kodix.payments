<?php

namespace Kodix\Payments\Exceptions;

use Bitrix\Main\Entity\EntityError;
use Bitrix\Main\Entity\FieldError;
use Bitrix\Main\SystemException;

class ModelException extends SystemException
{
    /**
     * @var EntityError[]|FieldError[]
     */
    private $errors;

    public function __construct(array $errors, $message = "", $code = 0, $file = "", $line = 0, \Exception $previous = null)
    {
        $this->errors = $errors;
        parent::__construct($message, $code, $file, $line, $previous);
    }

    /**
     * Returns errors.
     *
     * @return EntityError[]|FieldError[]
     */
    public function getErrors()
    {
        return $this->errors;
    }
}