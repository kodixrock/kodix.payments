<?php

namespace Kodix\Payments\Exceptions;

use Bitrix\Main\SystemException;

class ProviderException extends SystemException
{
}