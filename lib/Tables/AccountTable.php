<?php

namespace Kodix\Payments\Tables;

use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Entity\Validator\Length;
use Bitrix\Main\Entity\Validator\Unique;
use Bitrix\Main\Type\DateTime;
use Kodix\Payments\JWT\JWT;

class AccountTable extends DataManager
{
    /** @inheritdoc */
    public static function getTableName()
    {
        return 'kodix_payments_account';
    }

    /** @inheritdoc */
    public static function getMap()
    {
        return [
            'ID' => [
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => 'ID',
            ],
            'LOGIN' => [
                'data_type' => 'string',
                'required' => true,
                'validation' => function () {
                    return [new Unique(), new Length(null, 64)];
                },
                'title' => 'Login',
            ],
            'SECRET' => [
                'data_type' => 'string',
                'required' => true,
                'validation' => function () {
                    return [new Length(null, 128)];
                },
                'title' => 'Secret',
            ],
            'ACTIVE' => [
                'data_type' => 'boolean',
                'values' => ['N', 'Y'],
                'required' => true,
                'default_value' => 'Y',
                'title' => 'Active',
            ],
            'LOGINED_AT' => [
                'data_type' => 'datetime',
                'required' => false,
                'title' => 'Last login',
            ],
            'CREATED_AT' => [
                'data_type' => 'datetime',
                'title' => 'Created at',
            ],
            'UPDATED_AT' => [
                'data_type' => 'datetime',
                'title' => 'Updated at',
            ],
        ];
    }

    /** @inheritdoc */
    public static function add(array $data)
    {
        $now = new DateTime();
        $data['CREATED_AT'] = $now;
        $data['UPDATED_AT'] = $now;

        return parent::add($data);
    }

    /** @inheritdoc */
    public static function update($primary, array $data)
    {
        unset($data['CREATED_AT']);
        $data['UPDATED_AT'] = new DateTime();

        return parent::update($primary, $data);
    }

    /**
     * Returns secret's hash
     *
     * @param string $secret
     * @return bool|string
     */
    public static function getHash($secret)
    {
        return password_hash($secret, PASSWORD_BCRYPT);
    }

    /**
     * Returns account by login and optional secret for authentication.
     *
     * @param string $login Account login
     * @param string $secret Account secret
     * @return array|false
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public static function getByLogin($login, $secret = null)
    {
        $account = self::getList(['filter' => ['LOGIN' => $login, 'ACTIVE' => 'Y']])->fetch();

        if (is_array($account) && !is_null($secret) && !password_verify($secret, $account['SECRET'])) {
            return false;
        }
        return $account;
    }

    /**
     * Returns JWT token
     *
     * @param array $payload
     * @param string $privateKey
     * @return string
     */
    public static function getToken(array $payload, $privateKey)
    {
        $payload = array_merge([
        ], $payload);
        return JWT::encode($payload, $privateKey, 'RS256');
    }
}