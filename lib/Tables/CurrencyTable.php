<?php

namespace Kodix\Payments\Tables;

use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Entity\Validator\Length;
use Bitrix\Main\Entity\Validator\Unique;
use Bitrix\Main\Type\DateTime;

class CurrencyTable extends DataManager
{
    /** @inheritdoc */
    public static function getTableName()
    {
        return 'kodix_payments_currency';
    }

    /** @inheritdoc */
    public static function getMap()
    {
        return [
            'ID' => [
                'data_type' => 'string',
                'primary' => true,
                'validation' => function () {
                    return [new Length(null, 5)];
                },
                'title' => 'ID',
            ],
            'NAME' => [
                'data_type' => 'string',
                'required' => true,
                'validation' => function () {
                    return [new Unique()];
                },
                'title' => 'Name',
            ],
            'ISO' => [
                'data_type' => 'string',
                'required' => true,
                'title' => 'ISO Code',
            ],
            'SHORT_NAME' => [
                'data_type' => 'string',
                'required' => false,
                'validation' => function () {
                    return [new Unique()];
                },
                'title' => 'Short name',
            ],
            'NOMINAL' => [
                'data_type' => 'float',
                'required' => true,
                'default_value' => 1,
                'title' => 'Nominal',
            ],
            'VALUE' => [
                'data_type' => 'float',
                'required' => true,
                'default_value' => 1,
                'title' => 'Value',
            ],
            'CREATED_AT' => [
                'data_type' => 'datetime',
                'title' => 'Created at',
            ],
            'UPDATED_AT' => [
                'data_type' => 'datetime',
                'title' => 'Updated at',
            ],
        ];
    }

    /** @inheritdoc */
    public static function add(array $data)
    {
        $now = new DateTime();
        $data['CREATED_AT'] = $now;
        $data['UPDATED_AT'] = $now;

        return parent::add($data);
    }

    /** @inheritdoc */
    public static function update($primary, array $data)
    {
        unset($data['CREATED_AT']);
        $data['UPDATED_AT'] = new DateTime();

        return parent::update($primary, $data);
    }

}