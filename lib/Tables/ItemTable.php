<?php

namespace Kodix\Payments\Tables;

use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Entity\Validator\Foreign;
use Bitrix\Main\Entity\Validator\Range;
use Bitrix\Main\Type\DateTime;

class ItemTable extends DataManager
{
    /** @inheritdoc */
    public static function getTableName()
    {
        return 'kodix_payments_order_item';
    }

    /** @inheritdoc */
    public static function getMap()
    {
        return [
            'ID' => [
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => 'ID',
            ],
            'EXTERNAL_ID' => [
                'data_type' => 'string',
                'required' => false,
                'title' => 'External ID',
            ],
            'ORDER_ID' => [
                'data_type' => 'string',
                'required' => true,
                'validation' => function () {
                    return [new Foreign(OrderTable::getEntity()->getField('ID'))];
                },
                'title' => 'Order ID',
            ],
            'ORDER' => [
                'data_type' => OrderTable::class,
                'reference' => [
                    '=this.ORDER_ID' => 'ref.ID',
                ],
                'title' => 'Order',
            ],
            'NAME' => [
                'data_type' => 'string',
                'required' => true,
                'title' => 'Name',
            ],
            'PRICE' => [
                'data_type' => 'float',
                'required' => true,
                'title' => 'Price',
            ],
            'CURRENCY_ID' => [
                'data_type' => 'string',
                'required' => true,
                'validation' => function () {
                    return [new Foreign(CurrencyTable::getEntity()->getField('ID'))];
                },
                'title' => 'Currency ID',
            ],
            'CURRENCY' => [
                'data_type' => CurrencyTable::class,
                'reference' => [
                    '=this.CURRENCY_ID' => 'ref.ID',
                ],
                'title' => 'Currency',
            ],
            'COUNT' => [
                'data_type' => 'float',
                'required' => true,
                'validation' => function () {
                    return [new Range(0, null)];
                },
                'title' => 'Count'
            ],
            'CREATED_AT' => [
                'data_type' => 'datetime',
                'title' => 'Created at',
            ],
            'UPDATED_AT' => [
                'data_type' => 'datetime',
                'title' => 'Updated at',
            ],
        ];
    }

    /** @inheritdoc */
    public static function add(array $data)
    {
        $now = new DateTime();
        $data['CREATED_AT'] = $now;
        $data['UPDATED_AT'] = $now;

        return parent::add($data);
    }

    /** @inheritdoc */
    public static function update($primary, array $data)
    {
        unset($data['CREATED_AT']);
        $data['UPDATED_AT'] = new DateTime();

        return parent::update($primary, $data);
    }
}