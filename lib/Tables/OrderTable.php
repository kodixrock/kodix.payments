<?php

namespace Kodix\Payments\Tables;

use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Entity\FieldError;
use Bitrix\Main\Entity\Validator\Enum;
use Bitrix\Main\Entity\Validator\Foreign;
use Bitrix\Main\Entity\Validator\Range;
use Bitrix\Main\Type\DateTime;

class OrderTable extends DataManager
{
    const STATUS_NEW = 'new';
    const STATUS_CREATED = 'created';
    const STATUS_PAID = 'paid';
    const STATUS_CANCELED = 'canceled';
    const STATUS_CONFIRMED = 'confirmed';
    const STATUS_FAILED = 'failed';

    const STATUSES = [
        self::STATUS_NEW,
        self::STATUS_CREATED,
        self::STATUS_PAID,
        self::STATUS_CANCELED,
        self::STATUS_CONFIRMED,
        self::STATUS_FAILED,
    ];

    /** @inheritdoc */
    public static function getTableName()
    {
        return 'kodix_payments_order';
    }

    /** @inheritdoc */
    public static function getMap()
    {
        return [
            'ID' => [
                'data_type' => 'string',
                'primary' => true,
                'title' => 'ID',
            ],
            'EXTERNAL_ID' => [
                'data_type' => 'string',
                'required' => false,
                'title' => 'External ID',
            ],
            'PAYMENT_ID' => [
                'data_type' => 'string',
                'required' => false,
                'title' => 'Payment ID',
            ],
            'ACCOUNT_ID' => [
                'data_type' => 'integer',
                'required' => true,
                'validation' => function () {
                    return [new Foreign(AccountTable::getEntity()->getField('ID'))];
                },
                'title' => 'Account ID',
            ],
            'ACCOUNT' => [
                'data_type' => AccountTable::class,
                'reference' => [
                    '=this.ACCOUNT_ID' => 'ref.ID',
                ],
                'title' => 'Account',
            ],
            'MERCHANT_ID' => [
                'data_type' => 'integer',
                'required' => false,
//                'validation' => function () {
//                    return [new Foreign(MerchantTable::getEntity()->getField('ID'))];
//                },
                'title' => 'Merchant ID',
            ],
            'MERCHANT' => [
                'data_type' => MerchantTable::class,
                'reference' => [
                    '=this.MERCHANT_ID' => 'ref.ID',
                ],
                'title' => 'Account',
            ],
            'CLIENT_NAME' => [
                'data_type' => 'string',
                'required' => false,
                'title' => 'Client name',
            ],
            'CLIENT_SURNAME' => [
                'data_type' => 'string',
                'required' => false,
                'title' => 'Client surname',
            ],
            'CLIENT_MOBILE' => [
                'data_type' => 'string',
                'required' => false,
                'title' => 'Client mobile',
            ],
            'CLIENT_EMAIL' => [
                'data_type' => 'string',
                'required' => false,
                'title' => 'Client email',
            ],
            'AMOUNT' => [
                'data_type' => 'float',
                'required' => true,
                'validation' => function () {
                    return [new Range(0, null, true)];
                },
                'title' => 'Order amount',
            ],
            'CURRENCY_ID' => [
                'data_type' => 'string',
                'required' => true,
                'validation' => function () {
                    return [new Foreign(CurrencyTable::getEntity()->getField('ID'))];
                },
                'title' => 'Currency ID',
            ],
            'CURRENCY' => [
                'data_type' => CurrencyTable::class,
                'reference' => [
                    '=this.CURRENCY_ID' => 'ref.ID',
                ],
                'title' => 'Currency',
            ],
            'DESCRIPTION' => [
                'data_type' => 'string',
                'required' => false,
                'title' => 'Description',
            ],
            'STATUS' => [
                'data_type' => 'enum',
                'required' => true,
                'values' => self::STATUSES,
                'validation' => function () {
                    return [new Enum()];
                },
                'default_value' => self::STATUS_NEW,
                'title' => 'Status',
            ],
            'PAID_AT' => [
                'data_type' => 'datetime',
                'title' => 'Paid at',
            ],
            'PAYMENT_URL' => [
                'data_type' => 'string',
                'title' => 'Payment URL',
            ],
            'SUCCESS_URL' => [
                'data_type' => 'string',
                'required' => true,
                'title' => 'Success URL',
            ],
            'FAIL_URL' => [
                'data_type' => 'string',
                'title' => 'Fail URL',
            ],
            'CREATED_AT' => [
                'data_type' => 'datetime',
                'title' => 'Created at',
            ],
            'UPDATED_AT' => [
                'data_type' => 'datetime',
                'title' => 'Updated at',
            ],
        ];
    }

    /** @inheritdoc */
    public static function add(array $data)
    {
        while (true) {
            $now = new DateTime();
            $data['CREATED_AT'] = $now;
            $data['UPDATED_AT'] = $now;

            $data['ID'] = static::uuid();

            $result = parent::add($data);
            foreach ($result->getErrors() as $error) {
                if ($error instanceof FieldError && $error->getField()->getName() == 'ID') {
                    continue 2;
                }
            }
            return $result;
        }
    }

    /** @inheritdoc */
    public static function update($primary, array $data)
    {
        unset($data['CREATED_AT']);
        $data['UPDATED_AT'] = new DateTime();

        return parent::update($primary, $data);
    }

    /**
     * Creates and returns new UUID.
     *
     * @return string
     */
    private static function uuid()
    {
        return uniqid();
    }
}