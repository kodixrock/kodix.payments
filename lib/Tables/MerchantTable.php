<?php

namespace Kodix\Payments\Tables;

use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Entity\Validator\Foreign;
use Bitrix\Main\FileTable;
use Bitrix\Main\Type\DateTime;

class MerchantTable extends DataManager
{
    /** @inheritdoc */
    public static function getTableName()
    {
        return 'kodix_payments_merchant';
    }

    /** @inheritdoc */
    public static function getMap()
    {
        return [
            'ID' => [
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => 'ID',
            ],
            'NAME' => [
                'data_type' => 'string',
                'required' => true,
                'title' => 'Name',
            ],
            'IMAGE_ID' => [
                'data_type' => 'integer',
                'required' => false,
                'validation' => function () {
                    return [new Foreign(FileTable::getEntity()->getField('ID'))];
                },
                'title' => 'Image ID',
            ],
            'IMAGE' => [
                'data_type' => FileTable::class,
                'reference' => [
                    '=this.IMAGE_ID' => 'ref.ID',
                ],
                'title' => 'Image',
            ],
            'ACTIVE' => [
                'data_type' => 'boolean',
                'values' => ['N', 'Y'],
                'required' => true,
                'default_value' => 'Y',
                'title' => 'Active',
            ],
            'PROVIDER' => [
                'data_type' => 'string',
                'required' => true,
                'title' => 'Provider',
            ],
            'CONFIG' => [
                'data_type' => 'string',
                'required' => false,
                'title' => 'Configuration',
            ],
            'SORT' => [
                'data_type' => 'integer',
                'required' => true,
                'default_value' => 500,
                'title' => 'Sort',
            ],
            'CREATED_AT' => [
                'data_type' => 'datetime',
                'title' => 'Created at',
            ],
            'UPDATED_AT' => [
                'data_type' => 'datetime',
                'title' => 'Updated at',
            ],
        ];
    }

    /** @inheritdoc */
    public static function add(array $data)
    {
        $now = new DateTime();
        $data['CREATED_AT'] = $now;
        $data['UPDATED_AT'] = $now;

        return parent::add($data);
    }

    /** @inheritdoc */
    public static function update($primary, array $data)
    {
        unset($data['CREATED_AT']);
        $data['UPDATED_AT'] = new DateTime();

        return parent::update($primary, $data);
    }
}