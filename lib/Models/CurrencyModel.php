<?php

namespace Kodix\Payments\Models;

use Kodix\Payments\Model;
use Kodix\Payments\Tables\CurrencyTable;

/**
 * Class CurrencyModel
 * @package Kodix\Payments\Models
 */
class CurrencyModel extends Model
{
    /** @inheritdoc */
    public static function getTable()
    {
        return CurrencyTable::class;
    }

    /**
     * @json id,string
     * @table ID,string
     * @var string
     */
    public $id;

    /**
     * @json name,string
     * @table NAME,string
     * @var string
     */
    public $name;

    /**
     * @json iso,string
     * @table ISO,string
     * @var string
     */
    public $iso;

    /**
     * @json shortName,string
     * @table SHORT_NAME,string
     * @var string
     */
    public $shortName;

    /**
     * @json nominal,double
     * @table NOMINAL,double
     * @var double
     */
    public $nominal;

    /**
     * @json value,double
     * @table VALUE,double
     * @var double
     */
    public $value;

    /**
     * @json created,timestamp
     * @table CREATED_AT,datetime,omitwrite
     * @var mixed
     */
    public $created;

    /**
     * @json updated,timestamp
     * @table UPDATED_AT,datetime
     * @var mixed
     */
    public $updated;

    /**
     * Converts price to another currency.
     *
     * @param float $price
     * @param CurrencyModel $currency
     * @return float
     */
    public function convertTo($price, CurrencyModel $currency)
    {
        $k = ($this->value / $this->nominal) / ($currency->value / $currency->nominal);
        return $price * $k;
    }
}