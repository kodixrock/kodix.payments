<?php

namespace Kodix\Payments\Models;

use Bitrix\Main\Application;
use Bitrix\Main\Error;
use COption;
use Kodix\Payments\Exceptions\ModelException;
use Kodix\Payments\Model;
use Kodix\Payments\Tables\OrderTable;

/**
 * Class OrderModel
 * @package Kodix\Payments\Models
 */
class OrderModel extends Model
{
    /** @inheritdoc */
    public static function getTable()
    {
        return OrderTable::class;
    }

    /**
     * @json id,string
     * @table ID,string,omitwrite
     * @var string
     */
    protected $id;

    /**
     * @json extId,string
     * @table EXTERNAL_ID,string
     * @var string
     */
    public $extId;

    /**
     * @json paymentId,string
     * @table PAYMENT_ID,string
     * @var string
     */
    public $paymentId;

    /**
     * @table ACCOUNT_ID,integer
     * @var integer
     */
    public $accountId;

    /**
     * @table MERCHANT_ID,integer
     * @var integer
     */
    protected $merchantId;

    /**
     * @var MerchantModel
     */
    protected $merchant;

    /**
     * @json clientName,string
     * @table CLIENT_NAME,string
     * @var string
     */
    public $clientName;

    /**
     * @json clientSurname,string
     * @table CLIENT_SURNAME,string
     * @var string
     */
    public $clientSurname;

    /**
     * @json clientMobile,string
     * @table CLIENT_MOBILE,string
     * @var string
     */
    public $clientMobile;

    /**
     * @json clientEmail,string
     * @table CLIENT_EMAIL,string
     * @var string
     */
    public $clientEmail;

    /**
     * @json amount,double
     * @table AMOUNT,double
     * @var double
     */
    public $amount;

    /**
     * @json currency,string
     * @table CURRENCY_ID,string
     * @var string
     */
    protected $currencyId;

    /**
     * @json description,string
     * @table DESCRIPTION,string
     * @var string
     */
    public $description;

    /**
     * @var CurrencyModel
     */
    protected $currency;

    /**
     * @json status,string
     * @table STATUS,string
     * @var string
     */
    public $status;

    /**
     * @json paid,timestamp
     * @table PAID_AT,datetime
     * @var mixed
     */
    public $paidAt;

    /**
     * @json paymentUrl,string
     * @table PAYMENT_URL,string
     * @var string
     */
    public $paymentUrl;

    /**
     * @json successUrl,string
     * @table SUCCESS_URL,string
     * @var string
     */
    public $successUrl;

    /**
     * @json failUrl,string
     * @table FAIL_URL,string
     * @var string
     */
    public $failUrl;

    /**
     * @json created,timestamp
     * @table CREATED_AT,datetime,omitwrite
     * @var mixed
     */
    public $created;

    /**
     * @json updated,timestamp
     * @table UPDATED_AT,datetime
     * @var mixed
     */
    public $updated;

    /**
     * @json items
     * @var ItemModel[]
     */
    protected $items = [];

    /**
     * ID getter.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * ID setter.
     *
     * @param string $value
     */
    public function setId($value)
    {
        if ($this->id != $value) {
            $this->items = null;
        }
        $this->id = $value;
    }

    /**
     * Merchant ID getter.
     *
     * @return int
     */
    public function getMerchantId()
    {
        return $this->merchantId;
    }

    /**
     * Merchant ID setter.
     *
     * @param int $value
     */
    public function setMerchantId($value)
    {
        if ($value != $this->merchantId) {
            $this->merchant = null;
        }
        $this->merchantId = $value;
    }

    /**
     * Merchant ID getter.
     *
     * @return MerchantModel|null
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function getMerchant()
    {
        if (is_null($this->merchant) && (int)$this->merchantId > 0) {
            $this->merchant = MerchantModel::getById($this->merchantId);
        }
        return $this->merchant;
    }

    /**
     * Currency ID getter.
     *
     * @return string
     */
    public function getCurrencyId()
    {
        return $this->currencyId;
    }

    /**
     * Currency ID setter.
     *
     * @param string $value
     */
    public function setCurrencyId($value)
    {
        if ($value != $this->currencyId) {
            $this->currency = null;
        }
        $this->currencyId = $value;
    }

    /**
     * Currency getter.
     *
     * @return CurrencyModel|null
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function getCurrency()
    {
        if (is_null($this->currency) && strlen($this->currencyId)) {
            $this->currency = CurrencyModel::getById($this->currencyId);
        }
        return $this->currency;
    }

    /**
     * @return ItemModel[]
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function getItems()
    {
        if (empty($this->items) && strlen($this->id)) {
            foreach (ItemModel::getList(['filter' => ['ORDER_ID' => $this->id]]) as $item) {
                $this->items[] = $item;
            };
        }
        return $this->items;
    }

    /**
     * Items setter.
     *
     * @param mixed $items
     */
    public function setItems(array $items)
    {
        $this->items = [];
        foreach ($items as $item) {
            $this->items[] = is_subclass_of($item, Model::class) ? $item : ItemModel::json($item);
        }
    }

    /** @inheritdoc */
    public function save()
    {
        $currency = $this->getCurrency();
        $amount = 0;
        $items = [];
        foreach ($this->getItems() as $item) {
            $items[$item->id] = $item;
        }

        foreach ($items as $item) {
            if (!strlen($item->getCurrencyId())) {
                $item->setCurrencyId(COption::GetOptionString('kodix.payments', 'currency'));
            }

            $itemCurrency = $item->getCurrency();
            if (is_null($itemCurrency)) {
                $message = strlen($item->getCurrencyId()) ?
                    sprintf('invalid currency %s', $item->getCurrencyId()) :
                    sprintf('required field currency is empty');
                throw new ModelException([new Error($message)]);
            }

            if (is_null($currency)) {
                $amount = $item->price * $item->count;
                $currency = $itemCurrency;
            } else {
                $amount += $itemCurrency->convertTo($item->price * $item->count, $currency);
            }
        }

        $this->amount = $amount;
        $this->currencyId = $currency->id;

        $db = Application::getConnection();
        $db->startTransaction();

        try {
            foreach (ItemModel::getList(['filter' => ['ORDER_ID' => $this->getId()]]) as $item) {
                if (!isset($items[$item->id])) {
                    $item->remove();
                }
            }

            parent::save();

            foreach ($items as &$item) {
                $item->orderId = $this->getId();
                $item->save();
            }
        } catch (\Exception $e) {
            $db->rollbackTransaction();
            throw $e;
        }

        $db->commitTransaction();

        return $this;
    }
}