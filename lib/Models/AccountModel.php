<?php

namespace Kodix\Payments\Models;

use Bitrix\Main\Type\DateTime;
use Kodix\Payments\Model;
use Kodix\Payments\Tables\AccountTable;

class AccountModel extends Model
{
    /**
     * Returns new RSA key pair in PEM format.
     *
     * @return array.
     * @throws \Exception
     */
    public static function getRSAKeys()
    {
        if (extension_loaded('openssl')) {
            $key = openssl_pkey_new();
            $public = openssl_pkey_get_details($key)['key'];
            $private = null;
            openssl_pkey_export($key, $private);
            openssl_free_key($key);
            if (is_null($private) || empty($public)) {
                throw new \Exception('Cannot issue RSA keys.');
            }
            return [$public, $private];
        } else {
            throw new \Exception('PHP extension [openssl] not installed.');
        }
    }

    /**
     * Validates RSA key pair.
     *
     * @param string $public
     * @param string $private
     * @return bool
     * @throws \Exception
     */
    public static function validateRSAKeys($public, $private)
    {
        if (extension_loaded('openssl')) {
            $origin = 'test';
            $encrypted = '';
            if (openssl_public_encrypt($origin, $encrypted, $public)) {
                $decrypted = '';
                if (openssl_private_decrypt($encrypted, $decrypted, $private)) {
                    return $decrypted == $origin;
                }
            }
            return false;
        } else {
            throw new \Exception('PHP extension [openssl] not installed.');
        }
    }

    /** @inheritdoc */
    public static function getTable()
    {
        return AccountTable::class;
    }

    /**
     * Returns account by login and optional secret for authentication.
     *
     * @param string $login
     * @param string $secret
     * @return AccountModel
     */
    public static function getByLogin($login, $secret = null)
    {
        $table = static::getTable();
        $row = $table::getByLogin($login, $secret);
        if (is_array($row)) {
            $result = new static();
            $result->fromDb($row);
            return $result;
        }
        return $row;
    }

    /**
     * @json id,integer
     * @table ID,integer
     * @var integer
     */
    public $id;

    /**
     * @json login,string
     * @table LOGIN,string
     * @var string
     */
    public $login;

    /**
     * @json secret,string
     * @table SECRET,string,omitread
     * @var string
     */
    private $secret;

    /**
     * @json active,boolean
     * @table ACTIVE,boolean
     * @var boolean
     */
    public $active;

    /**
     * @json logined,timestamp
     * @table LOGINED_AT,datetime
     * @var mixed
     */
    public $logined;

    /**
     * @json created,timestamp
     * @table CREATED_AT,datetime
     * @var mixed
     */
    public $created;

    /**
     * @json updated,timestamp
     * @table UPDATED_AT,datetime
     * @var mixed
     */
    public $updated;

    public function __construct($login = null, $secret = null)
    {
        $this->login = $login;
        $this->secret = $secret;
    }

    /**
     * Secret getter.
     *
     * @return string
     */
    public function getSecret()
    {
        $table = static::getTable();

        return strlen($this->secret) ? $table::getHash($this->secret) : $this->origin['SECRET'];
    }

    /**
     * Secret setter.
     *
     * @param string $value
     */
    public function setSecret($value)
    {
        $this->secret = $value;
    }

    /**
     * Returns JWT token.
     *
     * @return mixed
     * @throws \Bitrix\Main\ObjectException
     */
    public function getToken()
    {
        $table = static::getTable();
        $now = new DateTime(null, null, new \DateTimeZone('UTC'));
        $payload = [
            'sub' => (string)$this->id,
            'iat' => $now->getTimestamp(),
            'exp' => $now->add("+1 hour")->getTimestamp(),
        ];
        return $table::getToken($payload, \COption::GetOptionString('kodix.payments', 'private_key'));
    }
}
