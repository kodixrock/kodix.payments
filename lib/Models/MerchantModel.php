<?php

namespace Kodix\Payments\Models;

use Bitrix\Main\FileTable;
use Kodix\Payments\Model;
use Kodix\Payments\Tables\MerchantTable;

/**
 * Class MerchantModel
 * @package Kodix\Payments\Models
 */
class MerchantModel extends Model
{
    /** @inheritdoc */
    public static function getTable()
    {
        return MerchantTable::class;
    }

    /**
     * @json id
     * @table ID,int
     * @var integer
     */
    public $id;

    /**
     * @json name
     * @table NAME,string
     * @var string
     */
    public $name;

    /**
     * @table IMAGE_ID
     * @var integer
     */
    private $imageId;

    /**
     * @json image,string
     * @var array
     */
    public $image;

    /**
     * @json active,boolean
     * @table ACTIVE,bool
     * @var boolean
     */
    public $active;

    /**
     * @json provider,string
     * @table PROVIDER
     * @var string
     */
    public $provider;

    /**
     * @table CONFIG,json
     * @var array
     */
    public $config;

    /**
     * @json sort,integer
     * @table SORT,integer
     * @var integer
     */
    public $sort;

    /**
     * @json created,timestamp
     * @table CREATED_AT,datetime,omitwrite
     * @var mixed
     */
    public $created;

    /**
     * @json updated,timestamp
     * @table UPDATED_AT,datetime
     * @var mixed
     */
    public $updated;

    /**
     * Image ID getter.
     *
     * @return int
     */
    public function getImageId()
    {
        return $this->imageId;
    }

    public function setImageId($value)
    {
        $path = \CFile::GetPath($value);
        if (!is_null($path)) {
            $this->imageId = $value;
            $this->image = $path;
        }
    }


}