<?php

namespace Kodix\Payments\Models;

use Kodix\Payments\Model;
use Kodix\Payments\Tables\ItemTable;

/**
 * Class ItemModel
 * @package Kodix\Payments\Models
 */
class ItemModel extends Model
{
    public static function getTable()
    {
        return ItemTable::class;
    }

    /**
     * @table ID,integer,omitwrite
     * @var integer
     */
    public $id;

    /**
     * @json extId,string
     * @table EXTERNAL_ID,string
     * @var string
     */
    public $externalId;

    /**
     * @table ORDER_ID,string
     * @var string
     */
    public $orderId;

    /**
     * @json name,string
     * @table NAME,string
     * @var string
     */
    public $name;

    /**
     * @json price,double
     * @table PRICE,double
     * @var double
     */
    public $price;

    /**
     * @json currency,string
     * @table CURRENCY_ID,string
     * @var string
     */
    protected $currencyId;

    /**
     * @var CurrencyModel
     */
    protected $currency;

    /**
     * @json count,double
     * @table COUNT,double
     * @var double
     */
    public $count;

    /**
     * @json created,timestamp
     * @table CREATED_AT,datetime,omitwrite
     * @var mixed
     */
    public $created;

    /**
     * @json updated,timestamp
     * @table UPDATED_AT,datetime
     * @var mixed
     */
    public $updated;

    /**
     * Currency ID getter.
     *
     * @return string
     */
    public function getCurrencyId()
    {
        return $this->currencyId;
    }

    /**
     * Currency ID setter.
     *
     * @param string $value
     */
    public function setCurrencyId($value)
    {
        if ($value != $this->currencyId) {
            $this->currency = null;
        }
        $this->currencyId = $value;
    }

    /**
     * Currency getter.
     *
     * @return CurrencyModel|null
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function getCurrency()
    {
        if (is_null($this->currency) && strlen($this->currencyId)) {
            $this->currency = CurrencyModel::getById($this->currencyId);
        }
        return $this->currency;
    }

}