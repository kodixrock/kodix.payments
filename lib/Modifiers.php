<?php

namespace Kodix\Payments;

trait Modifiers
{
    /**
     * Returns properties modifiers.
     *
     * @return array
     * @throws \ReflectionException
     */
    public function getProperties()
    {
        $result = [];
        $class = new \ReflectionClass($this);
        $props = $class->getProperties();
        foreach ($props as $prop) {
            $name = $prop->getName();
            $getter = 'get' . ucfirst($name);
            if (!method_exists($this, $getter) || !$class->getMethod($getter)->isPublic()) {
                $getter = null;
            }
            $setter = 'set' . ucfirst($name);
            if (!method_exists($this, $setter) || !$class->getMethod($setter)->isPublic()) {
                $setter = null;
            }

            $modifiers = $this->getModifiers($prop->getDocComment());

            $result[$name] = array_merge($modifiers, [
                'name' => $name,
                'public' => $prop->isPublic(),
                'getter' => $getter,
                'setter' => $setter,
            ]);
        }
        return $result;
    }

    /**
     * Return methods modifiers.
     *
     * @return array
     * @throws \ReflectionException
     */
    public function getMethods()
    {
        $result = [];
        $class = new \ReflectionClass($this);
        $methods = $class->getMethods();
        foreach ($methods as $method) {
            $name = $method->getName();
            $params = [];
            foreach ($method->getParameters() as $param) {
                $params[$param->getName()] = [
                    'name' => $param->getName(),
                    'value' => $param->isDefaultValueAvailable() ? $param->getDefaultValue() : null,
                ];
            };

            $modifiers = $this->getModifiers($method->getDocComment());

            $result[$name] = array_merge($modifiers, [
                'name' => $name,
                'public' => $method->isPublic(),
                'parameters' => $params,
            ]);
        }
        return $result;
    }

    /**
     * Returns modifiers from phpDoc comment.
     *
     * @param $docComment
     * @return array
     */
    public function getModifiers($docComment)
    {
        $result = [];
        $lines = explode("\n", $docComment);
        foreach ($lines as $line) {
            $line = trim($line);
            if (preg_match('/@([^\s]+)\s+([^\s]+)/', trim($line), $matches)) {
                switch ($matches[1]) {
                    case 'return':
                        $result[$matches[1]] = explode('|', $matches[2]);
                        break;
                    case 'var':
                        $result[$matches[1]] = explode('|', $matches[2]);
                        break;
                    default:
                        $result[$matches[1]] = explode(',', $matches[2]);
                }
            }
        }
        return $result;
    }
}