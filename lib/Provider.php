<?php

namespace Kodix\Payments;


use Bitrix\Main\SystemException;
use Kodix\Payments\Exceptions\ProviderException;
use Kodix\Payments\Models\OrderModel;

/**
 * Class Provider
 * @package Kodix\Payments
 */
abstract class Provider
{
    /**
     * Registered providers.
     *
     * @var Provider[]
     */
    private static $providers = [];

    /**
     * Returns provider class.
     *
     * @param string $id Provider ID
     * @return Provider|null Return null if provider doesn't exist.
     */
    final public static function get($id)
    {
        return self::$providers[$id];
    }

    final public static function getList()
    {
        return self::$providers;
    }

    /**
     * Creates and returns new provider instances.
     *
     * @param string $id Provider ID
     * @param array $props Provider properties
     * @return Provider|null Return null if provider doesn't exist.
     */
    final public static function create($id, array $props)
    {
        if (array_key_exists($id, self::$providers)) {
            return new self::$providers[$id]($props);
        }
        return null;
    }

    /**
     * Registers provider.
     *
     * @param Provider[] $providers Provider classes
     * @throws SystemException
     */
    final public static function register(array $providers)
    {
        foreach ($providers as $provider) {
            if (!is_subclass_of($provider, self::class)) {
                throw new SystemException('Provider must be subclass of ' . self::class . '.');
            }
            $id = $provider::getId();
            if (!is_string($id) || is_null($id)) {
                throw new SystemException('Invalid provider id for ' . $provider . '.');
            }
            if (array_key_exists($id, self::$providers)) {
                throw new SystemException('Provider with ' . $id . ' already registered.');
            }
            self::$providers[$id] = $provider;
        }
    }

    /**
     * Returns provider id.
     *
     * @return string
     */
    public static function getId()
    {
        return null;
    }

    /**
     * Returns provider configs map.
     *
     * @return array
     */
    public static function getProperties()
    {
        return [];
    }

    /**
     * Creates new order.
     *
     * @param OrderModel $order
     * @return OrderModel Returns stored order model.
     * @throws ProviderException
     */
    abstract public function createOrder(OrderModel $order);

    /**
     * Updates order status.
     *
     * @param OrderModel $order
     * @return OrderModel Returns updated order model.
     * @throws ProviderException
     */
    abstract public function updateOrder(OrderModel $order);
}