<?php

namespace Kodix\Payments;

abstract class Controller extends \CBitrixComponent
{
    use Modifiers;

    /**
     * Returns current request context.
     *
     * @return Context
     */
    protected function getContext()
    {
        return Context::getInstance();
    }

    /** @inheritdoc */
    public function executeComponent()
    {
        $prefix = '/' . trim($this->arParams['FOLDER'], DIRECTORY_SEPARATOR);

        $request = $this->getContext()->getRequest();
        $requestPath = parse_url($request->getRequestUri(), PHP_URL_PATH);
        $requestMethod = strtolower($request->getRequestMethod());

        if (strpos($requestPath, $prefix) == 0) {
            $requestPath = substr($requestPath, strlen($prefix));
        }

        $routes = [];
        foreach ($this->getMethods() as $method) {
            if ($method['public'] && !empty($method['route'])) {
                $options = $method['route'];
                $path = $this->getPathPattern(array_shift($options));
                if (!is_null($path) && in_array($requestMethod, array_map('strtolower', $options))) {
                    $routes[$path][] = $method;
                }
            }
        }

        $action = null;
        $actionWeight = 0;
        foreach ($routes as $pattern => $methods) {
            if (preg_match($pattern, $requestPath, $matches)) {
                unset($matches[0]);
                $vars = [];
                foreach ($matches as $name => $value) {
                    if (is_string($name)) {
                        $vars[$name] = $value;
                    }
                }
                $weight = count($matches) - count($vars) * 2;
                if (is_null($action) || $weight > $actionWeight) {
                    $arguments = [];
                    foreach ($methods[0]['parameters'] as $name => $property) {
                        $arguments[] = isset($vars[$name]) ? $vars[$name] : $property['value'];
                    }
                    $action = [
                        'name' => $methods[0]['name'],
                        'args' => $arguments,
                        'options' => array_slice($methods[0]['route'], 1),
                    ];
                    $actionWeight = $weight;
                }
            }
        }

        if (is_null($action)) {
            $this->notFound();
        }

        $isRest = in_array('rest', array_map('strtolower', $action['options']));
        try {
            $isRest ?
                $this->display(call_user_func_array([$this, $action['name']], $action['args'])) :
                call_user_func_array([$this, $action['name']], $action['args']);
        } catch (\Exception $e) {
            if ($isRest) {
                $this->display(['message' => $e->getMessage()], 500);
            }
        }
    }

    /**
     * Displays 404 page.
     */
    protected function notFound()
    {
        $this->display(['message' => 'route not found'], 404);
    }

    /**
     * Formats and displays result.
     *
     * @param $result
     * @param int $code
     * @throws \Bitrix\Main\ArgumentNullException
     * @throws \Bitrix\Main\ArgumentOutOfRangeException
     */
    protected function display($result, $code = 200)
    {
        while (ob_get_level() > 0) {
            ob_end_clean();
        }
        $response = $this->getContext()->getResponse();
        $response->addHeader('Content-Type', 'application/json');
        switch (true) {
            case is_subclass_of($result, Model::class):
                $body = $result->toJsonString();
                break;
            default:
                $body = json_encode($result, JSON_UNESCAPED_UNICODE);
        }
        http_response_code($code);
        $response->flush($body);
        die();
    }

    /**
     * Converts bitrix path string to the regex pattern.
     *
     * @param string $path
     * @return null|string
     */
    protected function getPathPattern($path)
    {
        if (preg_match_all('/(?:#[^#]+#|[^\/]+)+/', $path, $matches) > 0) {
            $result = [];
            foreach ($matches[0] as $match) {
                if (substr($match, 0, 1) == '#' && substr($match, -1, 1) == '#') {
                    $match = trim($match, '#');
                    list($name, $pattern) = explode(':', $match, 2);
                    if (!strlen($pattern)) {
                        $pattern = '[^/]+';
                    }
                    $result[] = '(?P<' . preg_quote($name) . '>' . $pattern . ')';
                } else {
                    $result[] = '(' . preg_quote($match) . ')';
                }
            }
            return '#^/' . implode('/', $result) . '/?$#U';
        }
        return null;
    }
}