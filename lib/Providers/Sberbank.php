<?php

namespace Kodix\Payments\Providers;

use Bitrix\Main\Type\DateTime;
use Bitrix\Main\Web\HttpClient;
use Kodix\Payments\Exceptions\ProviderException;
use Kodix\Payments\Models\OrderModel;
use Kodix\Payments\Provider;
use Kodix\Payments\Tables\OrderTable;

class Sberbank extends Provider
{
    /** @inheritdoc */
    public static function getId()
    {
        return 'sberbank';
    }

    /** @inheritdoc */
    public static function getProperties()
    {
        return [
            'url' => [
                'title' => 'URL',
                'type' => 'string',
                'required' => true,
            ],
            'login' => [
                'title' => 'LOGIN',
                'type' => 'string',
                'required' => false,
            ],
            'secret' => [
                'title' => 'PASSWORD',
                'type' => 'secret',
                'required' => false,
            ],
            'twoStage' => [
                'title' => 'TWO_STAGE',
                'type' => 'boolean',
                'required' => false,
            ]
        ];
    }

    /**
     * Api url prefix.
     *
     * @var string
     */
    private $url;

    /**
     * Api login.
     *
     * @var string|null
     */
    private $login;

    /**
     * Api secret.
     *
     * @var string|null
     */
    private $secret;

    /**
     * Is two stage payment.
     *
     * @var boolean
     */
    private $twoStage;

    public function __construct(array $properties)
    {
        $this->url = rtrim($properties['url'], '/') . '/';
        $this->login = $properties['login'];
        $this->secret = $properties['secret'];
        $this->twoStage = $properties['twoStage'];
    }

    /** @inheritdoc */
    public function createOrder(OrderModel $order)
    {
        $action = $this->twoStage ? 'registerPreAuth.do' : 'register.do';
        $orderId = [$order->getId()];
        if (strlen($order->extId)) {
            array_unshift($orderId, $order->extId);
        }
        $result = $this->query($action, [
            'orderNumber' => implode('-',$orderId),
            'amount' => round($order->amount * 100, 0),
            'currency' => $order->getCurrency()->iso,
            'description' => $order->description,
            'returnUrl' => $order->successUrl,
            'failUrl' => $order->failUrl,
            'orderBundle' => json_encode($this->getBundle($order))
        ]);

        if (is_array($result)) {
            if (strlen($result['errorMessage'])) {
                throw new ProviderException($result['errorMessage']);
            }
            $order->paymentId = $result['orderId'];
            $order->paymentUrl = $result['formUrl'];
            $order->status = OrderTable::STATUS_CREATED;
        } else {
            throw new ProviderException("can not created order");
        }
        return $order;
    }

    /** @inheritdoc */
    public function updateOrder(OrderModel $order)
    {
        if ($order->status == OrderTable::STATUS_CREATED) {
            $result = $this->query(
                'getOrderStatus.do',
                ['orderId' => $order->paymentId]
            );
            if (is_array($result)) {
                switch (true) {
                    case in_array($result['OrderStatus'], [1, 2]):
                        $order->status = OrderTable::STATUS_PAID;
                        $order->paidAt = new DateTime();
                        break;
                    case in_array($result['ErrorCode'], [2, 6]):
                    case in_array($result['OrderStatus'], [3, 4, 6]):
                        $order->status = OrderTable::STATUS_CANCELED;
                        break;
                }
            } else {
                throw new ProviderException("can not check order");
            }
        }
        return $order;
    }

    private function query($action, array $arguments)
    {
        $arguments = array_merge([
            'userName' => $this->login,
            'password' => $this->secret,
            'language' => 'ru',
        ], $arguments);

        $client = new HttpClient();
        $url = $this->url . $action . '?' . http_build_query($arguments);

        if (!$client->query(HttpClient::HTTP_GET, $url)) {
            return null;
        }
        return json_decode($client->getResult(), true);
    }

    private function getBundle(OrderModel $order)
    {
        $result = [];

        if (strlen($order->clientEmail)) {
            $result['customerDetails']['email'] = $order->clientEmail;
        }

        $orderCurrency = $order->getCurrency();
        foreach ($order->getItems() as $i => $item) {
            $currency = $item->getCurrency();
            $price = round($currency->convertTo($item->price, $orderCurrency) * 100);
            $amount = round($price * $item->count);
            $result['cartItems']['items'][] = [
                'positionId' => $i + 1,
                'name' => $item->name,
                'quantity' => [
                    'value' => $item->count,
                    'measure' => 'items',
                ],
                'itemAmount' => $amount,
                'itemPrice' => $price,
                'itemCode' => strlen($item->externalId) ? $item->externalId : $item->id,
            ];
        }

        return $result;
    }
}