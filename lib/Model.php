<?php

namespace Kodix\Payments;

use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Entity\EntityError;
use Bitrix\Main\Type\Date;
use Bitrix\Main\Type\DateTime;
use Kodix\Payments\Exceptions\ModelException;
use DateTimeZone;
use Kodix\Payments\Models\OrderModel;

abstract class Model
{
    use Accessor;
    use Modifiers;

    /**
     * Returns model table class name.
     *
     * @return DataManager
     */
    public static function getTable()
    {
        return null;
    }

    /**
     * Returns selection by entity's primary key.
     *
     * @param mixed $id Primary key
     * @return static|null Returns null if not exists.
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public static function getById($id)
    {
        $table = static::getTable();
        $row = $table::getById($id)->fetch();
        if (!is_array($row)) {
            return null;
        }
        $result = new static();
        $result->fromDb($row);
        return $result;
    }

    /**
     * Executes the query and returns selection by parameters of the query.
     *
     * @param array $parameters
     * @return \Generator
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public static function getList(array $parameters = [])
    {
        $table = static::getTable();
        $select = [];
        foreach ($table::getMap() as $name => $field) {
            if (!isset($field['reference'])) {
                $select[] = $name;
            }
        }
        $parameters['select'] = $select;

        $r = $table::getList($parameters);
        while ($row = $r->fetch()) {
            $model = new static();
            $model->fromDb($row);
            yield $model;
        }
    }

    /**
     * Marshals json.
     *
     * @param string|array $json
     * @return static
     */
    public static function json($json)
    {
        $model = new static();
        return is_array($json) ? $model->fromJsonMap($json) : $model->fromJsonString($json);
    }

    /**
     * Contains origin database data.
     *
     * @var array
     */
    protected $origin = [];

    /**
     * Stores entity to database.
     *
     * @return $this
     * @throws ModelException
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     * @throws \ReflectionException
     */
    public function save()
    {
        $table = static::getTable();
        $data = $this->toDb();

        $primary = [];
        $exists = true;
        foreach ($table::getMap() as $name => $field) {
            if ($field['primary']) {
                $primary[$name] = $this->origin[$name];
                $exists &= !is_null($primary[$name]);
            }
        }

        $result = $exists ? $table::update($primary, $data) : $table::add($data);
        if (!$result->isSuccess()) {
            throw new ModelException($result->getErrors());
        }

        $data = $table::getById($result->getId())->fetch();
        if (!is_array($data)) {
            throw new ModelException([new EntityError('can not find entity after save')]);
        }
        $this->fromDb($data);
        return $this;
    }

    /**
     * Removes entity from database.
     *
     * @throws ModelException
     */
    public function remove()
    {
        $table = static::getTable();
        $primary = [];

        $exists = true;
        foreach ($table::getMap() as $name => $field) {
            if ($field['primary']) {
                $primary[$name] = $this->origin[$name];
                $exists &= !is_null($primary[$name]);
            }
        }

        if ($exists) {
            $result = $table::delete($primary);
            if (!$result->isSuccess()) {
                throw new ModelException($result->getErrors());
            }
        }
    }

    /**
     * Sets model's properties value from database row array.
     *
     * @param array $row
     * @throws \Bitrix\Main\ObjectException
     * @throws \ReflectionException
     */
    protected function fromDb(array $row)
    {
        $table = static::getTable();
        foreach ($table::getMap() as $name => $field) {
            if (!isset($field['reference'])) {
                $this->origin[$name] = $row[$name];
            }
        }

        foreach ($this->getProperties() as $name => $prop) {
            if (($prop['public'] || $prop['setter']) && !empty($prop['table'])) {
                $value = $row[$prop['table'][0]];
                $options = array_slice($prop['table'], 1);

                if (!is_null($value)) {
                    switch (true) {
                        case in_array('omitread', $options):
                            continue 2;
                        case in_array('integer', $options):
                        case in_array('int', $options):
                            $value = (int)$value;
                            break;
                        case in_array('float', $options):
                        case in_array('double', $options):
                            $value = (double)$value;
                            break;
                        case in_array('boolean', $options):
                        case in_array('bool', $options):
                            $value = in_array($value, [true, 1, '1', 'y', 'Y']);
                            break;
                        case in_array('date', $options):
                            $value = new Date($value);
                            break;
                        case in_array('time', $options):
                        case in_array('datetime', $options):
                            $value = new DateTime($value);
                            break;
                        case in_array('timestamp', $options):
                            $value = (new DateTime($value, null, new DateTimeZone('UTC')))->getTimestamp();
                            break;
                        case in_array('string', $options):
                            $value = (string)$value;
                            break;
                        case in_array('json', $options):
                            $value = json_decode($value, true);
                            break;
                    }
                }
                $prop['setter'] ? $this->{$prop['setter']}($value) : $this->$name = $value;
            }
        }
    }

    /**
     * Returns model's properties value as database row array.
     *
     * @return array
     * @throws \Bitrix\Main\ObjectException
     * @throws \ReflectionException
     */
    protected function toDb()
    {
        $table = static::getTable();
        $map = $table::getMap();
        $data = $this->origin;
        foreach ($this->getProperties() as $name => $prop) {
            if (($prop['public'] || $prop['getter']) && !empty($prop['table'])) {
                $dbName = $prop['table'][0];
                $value = $prop['getter'] ? $this->{$prop['getter']}() : $this->$name;
                $options = array_slice($prop['table'], 1);
                if (!is_null($value) || in_array('omitwrite', $options)) {
                    switch (true) {
                        case in_array('omitwrite', $options):
                            $value = $this->origin[$dbName];
                            break;
                        case in_array('integer', $options):
                        case in_array('int', $options):
                            $value = (int)$value;
                            break;
                        case in_array('float', $options):
                        case in_array('double', $options):
                            $value = (double)$value;
                            break;
                        case in_array('boolean', $options):
                        case in_array('bool', $options):
                            if (!empty($map[$dbName]['values'])) {
                                $values = $value ?
                                    [true, 1, '1', 'y', 'Y'] :
                                    [false, -1, 0, '0', 'n', 'N', 'f', 'F'];
                                $values = array_intersect($values, $map[$dbName]['values']);
                                if (!empty($values)) {
                                    $value = array_shift($values);
                                    break;
                                }
                            }
                            $value = in_array($value, [true, 1, '1', 'y', 'Y']);
                            break;
                        case in_array('date', $options):
                            $value = new Date($value);
                            break;
                        case in_array('time', $options):
                        case in_array('datetime', $options):
                            $value = new DateTime($value);
                            break;
                        case in_array('timestamp', $options):
                            $value = (new DateTime($value, null, new DateTimeZone('UTC')))->getTimestamp();
                            break;
                        case in_array('string', $options):
                            $value = (string)$value;
                            break;
                        case in_array('json', $options):
                            $value = json_encode($value, JSON_UNESCAPED_UNICODE);
                            break;
                    }
                    $data[$dbName] = $value;
                }
            }
        }
        return $data;
    }

    /**
     * Returns string json representation of the model.
     *
     * @return false|string
     * @throws \Bitrix\Main\ObjectException
     * @throws \ReflectionException
     */
    public function toJsonString()
    {
        return json_encode($this->toJsonMap(), JSON_UNESCAPED_UNICODE);
    }

    /**
     * Returns array json representation of the model.
     *
     * @return array
     * @throws \Bitrix\Main\ObjectException
     * @throws \ReflectionException
     */
    public function toJsonMap()
    {
        $result = [];
        foreach ($this->getProperties() as $name => $prop) {
            if (($prop['public'] || $prop['getter']) && !empty($prop['json'])) {
                $jsonName = $prop['json'][0];
                $value = $prop['getter'] ? $this->{$prop['getter']}() : $this->$name;
                $result[$jsonName] = $this->toJsonValue($value, array_slice($prop['json'], 1));
            }
        }

        return $result;
    }

    /**
     * Returns json representation for the specified value.
     *
     * @param mixed $value
     * @param array $options
     * @return mixed
     * @throws \Bitrix\Main\ObjectException
     */
    protected function toJsonValue($value, array $options = [])
    {
        $result = null;
        switch (true) {
            case is_null($value):
                return null;
            case is_array($value):
                $result = [];
                foreach ($value as $key => $item) {
                    $result[$key] = $this->toJsonValue($item, $options);
                }
                break;
            case is_subclass_of($value, self::class):
                $result = $value->toJsonMap();
                break;
            case in_array('integer', $options):
            case in_array('int', $options):
                $result = (int)$value;
                break;
            case in_array('float', $options):
            case in_array('double', $options):
                $result = (double)$value;
                break;
            case in_array('boolean', $options):
            case in_array('bool', $options):
                $result = in_array($value, [true, 1, '1', 'y', 'Y']);
                break;
            case in_array('date', $options):
                $result = new Date($value);
                break;
            case in_array('time', $options):
            case in_array('datetime', $options):
                $result = (new DateTime($value))->toString();
                break;
            case in_array('timestamp', $options):
                $result = (new DateTime($value, null, new DateTimeZone('UTC')))->getTimestamp();
                break;
            case in_array('string', $options):
                $result = (string)$value;
                break;
            default:
                $result = $value;
        }
        return $result;
    }

    /**
     * Sets model's properties value from json string.
     *
     * @param string $json
     * @throws ModelException
     * @throws \Bitrix\Main\ObjectException
     * @throws \ReflectionException
     * @return $this
     */
    public function fromJsonString($json)
    {
        $data = json_decode($json, true);
        if (is_array($data)) {
            return $this->fromJsonMap($data);
        }
        throw new ModelException([], "invalid json");
    }

    /**
     * Sets model's properties from json map.
     *
     * @param array $json
     * @return $this
     * @throws \Bitrix\Main\ObjectException
     * @throws \ReflectionException
     */
    public function fromJsonMap(array $json)
    {
        foreach ($this->getProperties() as $name => $prop) {
            if (($prop['public'] || $prop['setter']) && !empty($prop['json'])) {
                $jsonName = $prop['json'][0];
                $value = $this->fromJsonValue($json[$jsonName], array_slice($prop['json'], 1));
                $prop['setter'] ? $this->{$prop['setter']}($value) : $this->$name = $value;
            }
        }
        return $this;
    }

    /**
     * Returns value from json representation.
     *
     * @param mixed $value
     * @param array $options
     * @return mixed
     * @throws \Bitrix\Main\ObjectException
     */
    protected function fromJsonValue($value, $options = [])
    {
        $result = null;
        switch (true) {
            case is_null($value):
                return null;
            case is_array($value):
                $result = [];
                foreach ($value as $key => $item) {
                    $result[$key] = $this->fromJsonValue($item, $options);
                }
                break;
            case in_array('integer', $options):
            case in_array('int', $options):
                $result = (int)$value;
                break;
            case in_array('float', $options):
            case in_array('double', $options):
                $result = (double)$value;
                break;
            case in_array('boolean', $options):
            case in_array('bool', $options):
                $result = in_array($value, [true, 1, '1', 'y', 'Y']);
                break;
            case in_array('date', $options):
                $result = new Date($value);
                break;
            case in_array('time', $options):
            case in_array('datetime', $options):
                $result = new DateTime($value);
                break;
            case in_array('timestamp', $options):
                $result = (int)$value;
                break;
            case in_array('string', $options):
                $result = (string)$value;
                break;
            default:
                $result = $value;
        }
        return $result;
    }
}