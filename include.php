<?php

Bitrix\Main\Loader::registerAutoLoadClasses('kodix.payments', [
    'Kodix\Payments\Accessor' => 'lib/Accessor.php',
    'Kodix\Payments\Exceptions\ProviderException' => 'lib/Exceptions/ProviderException.php',
    'Kodix\Payments\Exceptions\ModelException' => 'lib/Exceptions/ModelException.php',
    'Kodix\Payments\Context' => 'lib/Context.php',
    'Kodix\Payments\Modifiers' => 'lib/Modifiers.php',
    'Kodix\Payments\Provider' => 'lib/Provider.php',
    'Kodix\Payments\Model' => 'lib/Model.php',
    'Kodix\Payments\Models\MerchantModel' => 'lib/Models/MerchantModel.php',
    'Kodix\Payments\Models\AccountModel' => 'lib/Models/AccountModel.php',
    'Kodix\Payments\Models\CurrencyModel' => 'lib/Models/CurrencyModel.php',
    'Kodix\Payments\Models\ItemModel' => 'lib/Models/ItemModel.php',
    'Kodix\Payments\Models\OrderModel' => 'lib/Models/OrderModel.php',
    'Kodix\Payments\Providers\Sberbank' => 'lib/Providers/Sberbank.php',
    'Kodix\Payments\Controller' => 'lib/Controller.php',
    'Kodix\Payments\JWT\BeforeValidException' => 'lib/JWT/BeforeValidException.php',
    'Kodix\Payments\JWT\JWT' => 'lib/JWT/JWT.php',
    'Kodix\Payments\JWT\SignatureInvalidException' => 'lib/JWT/SignatureInvalidException.php',
    'Kodix\Payments\JWT\ExpiredException' => 'lib/JWT/ExpiredException.php',
    'Kodix\Payments\Tables\CurrencyTable' => 'lib/Tables/CurrencyTable.php',
    'Kodix\Payments\Tables\OrderTable' => 'lib/Tables/OrderTable.php',
    'Kodix\Payments\Tables\ItemTable' => 'lib/Tables/ItemTable.php',
    'Kodix\Payments\Tables\MerchantTable' => 'lib/Tables/MerchantTable.php',
    'Kodix\Payments\Tables\AccountTable' => 'lib/Tables/AccountTable.php',
]);

Kodix\Payments\Provider::register([
    Kodix\Payments\Providers\Sberbank::class,
]);
