<?php

$MESS['KODIX_PAYMENTS_UNINSTALL_TITLE'] = 'Удаление Kodix Online Payments';
$MESS['KODIX_PAYMENTS_UNINSTALL_BACK'] = 'Назад';
$MESS['KODIX_PAYMENTS_UNINSTALL_MESSAGE'] = 'Это действие приведет к удалению всех данных модуля, включая историю заказов. Вы действительно хотите продолжить?';

$MESS['KODIX_PAYMENTS_UNINSTALL_TAB'] = 'Удаление';
$MESS['KODIX_PAYMENTS_UNINSTALL_TAB_TITLE'] = 'Удаление модуля';
$MESS['KODIX_PAYMENTS_UNINSTALL_BTN'] = 'Удалить';
$MESS['KODIX_PAYMENTS_UNINSTALL_BTN_TITLE'] = 'Удалить модуль';

$MESS['KODIX_PAYMENTS_UNINSTALL_ERR_INTERNAL'] = 'Не удалось удалить модуль.';