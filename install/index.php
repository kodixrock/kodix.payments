<?php

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Application;
use Bitrix\Main\HttpRequest;
use Bitrix\Main\UrlRewriter;
use Kodix\Payments\Models\AccountModel;
use Kodix\Payments\Models\CurrencyModel;

class kodix_payments extends \CModule
{
    const MODULE_ID = 'kodix.payments';

    private static $tables = [
        \Kodix\Payments\Tables\AccountTable::class,
        \Kodix\Payments\Tables\CurrencyTable::class,
        \Kodix\Payments\Tables\MerchantTable::class,
        \Kodix\Payments\Tables\OrderTable::class,
        \Kodix\Payments\Tables\ItemTable::class,
    ];

    private static $links = [
        '#root#/#folder#' => '#module#/public',
        '#root#/local/components/kodix.payments' => '#module#/components/',
        '#root#/local/templates/payments_vw' => '#module#/templates/payments_vw',
    ];

    public function __construct()
    {
        $info = include(__DIR__ . DIRECTORY_SEPARATOR . 'version.php');
        foreach ($info as $key => $value) {
            $this->$key = $value;
        }

        Loc::loadLanguageFile(__DIR__ . '/index.php');
        $this->MODULE_DESCRIPTION = GetMessage('KODIX_PAYMENTS_DESCRIPTION');
    }

    public function DoInstall()
    {
        global $USER, $APPLICATION;
        if (!IsModuleInstalled(self::MODULE_ID) && $USER->IsAdmin()) {
            Loc::loadLanguageFile(__DIR__ . '/install.php');
            spl_autoload_register([$this, 'autoload']);

            $errors = [];
            $request = Application::getInstance()->getContext()->getRequest();
            if ($request->isPost()) {
                $errors = $this->installAction($request);
                if (empty($errors)) {
                    RegisterModule(self::MODULE_ID);
                    $this->challenge(GetMessage('KODIX_PAYMENTS_COMPLETE_TITLE'), __DIR__ . '/pages/complete.php');
                    return;
                }
            }

            $this->challenge(GetMessage('KODIX_PAYMENTS_INSTALL_TITLE'), __DIR__ . '/pages/install.php', $errors);
        }
    }

    public function DoUninstall()
    {
        global $USER;
        if (IsModuleInstalled(self::MODULE_ID) && $USER->IsAdmin() && self::IncludeModule(self::MODULE_ID)) {
            Loc::loadLanguageFile(__DIR__ . '/uninstall.php');

            $errors = [];
            $request = Application::getInstance()->getContext()->getRequest();
            if ($request->isPost()) {
                try {
                    $this->UninstallTemplates();
                    $this->UninstallFiles();
                    $this->UninstallDB();
                    $this->UninstallAgents();
                    UnRegisterModule(self::MODULE_ID);
                    return;
                } catch (Exception $e) {
                    $errors = [GetMessage('KODIX_PAYMENTS_UNINSTALL_ERR_INTERNAL'), $e->getMessage()];
                }
            }

            $this->challenge(GetMessage('KODIX_PAYMENTS_UNINSTALL_TITLE'), __DIR__ . '/pages/uninstall.php', $errors);
        }
    }


    private function installAction(HttpRequest $request)
    {
        $errors = [];
        $public_key = $request->getPost('public_key');
        $private_key = $request->getPost('private_key');
        $folder = trim($request->getPost('folder'), DIRECTORY_SEPARATOR);
        $login = trim($request->getPost('login'));
        $secret = trim($request->getPost('secret'));

        if (!AccountModel::validateRSAKeys($public_key, $private_key)) {
            $errors[] = GetMessage('KODIX_PAYMENTS_INSTALL_ERR_KEYS');
        }
        if (file_exists(rtrim(Application::getDocumentRoot(), DIRECTORY_SEPARATOR) . '/' . $folder)) {
            $errors[] = GetMessage('KODIX_PAYMENTS_INSTALL_ERR_FOLDER');
        }
        if (strlen($login)) {
            if (strlen($secret) < 4 || strpos($secret, ':') !== false) {
                $errors[] = GetMessage('KODIX_PAYMENTS_INSTALL_ERR_SECRET');
            }
        }

        if (count($errors) > 0) {
            return $errors;
        }

        if (!COption::SetOptionString(self::MODULE_ID, 'public_key', $public_key)) {
            return [GetMessage('KODIX_PAYMENTS_INSTALL_ERR_INTERNAL')];
        }
        if (!COption::SetOptionString(self::MODULE_ID, 'private_key', $private_key)) {
            return [GetMessage('KODIX_PAYMENTS_INSTALL_ERR_INTERNAL')];
        }
        if (!COption::SetOptionString(self::MODULE_ID, 'folder', $folder)) {
            return [GetMessage('KODIX_PAYMENTS_INSTALL_ERR_INTERNAL')];
        }
        if (!COption::SetOptionString(self::MODULE_ID, 'currency', 'RUB')) {
            return [GetMessage('KODIX_PAYMENTS_INSTALL_ERR_INTERNAL')];
        }

        try {
            $this->InstallDB();
            $this->InstallFiles();
            $this->InstallTemplates();
            $this->InstallAgents();
        } catch (Exception $e) {
            return [GetMessage('KODIX_PAYMENTS_INSTALL_ERR_INTERNAL'), $e->getMessage()];
        }

        try {
            GLOBAL $ACCOUNT;
            if (strlen($login)) {
                $account = new AccountModel($login, $secret);
                $account->save();
                $ACCOUNT = ['login' => $login, 'secret' => $secret];
            } else {
                $ACCOUNT = null;
            }
        } catch (Exception $e) {
        }

        try {
            $currency = new CurrencyModel();
            $currency->id = 'RUB';
            $currency->name = 'Российский рубль';
            $currency->shortName = 'руб.';
            $currency->iso = '643';
            $currency->nominal = 1;
            $currency->value = 1;
            $currency->save();
        } catch (Exception $e) {
        }

        return [];
    }

    private function challenge($title, $path, $errors = [])
    {
        global $APPLICATION, $ERRORS;
        $ERRORS = $errors;
        $APPLICATION->IncludeAdminFile($title, $path);
    }

    public function InstallDB()
    {
        /** @var $table \Bitrix\Main\Entity\DataManager */
        foreach (self::$tables as $table) {
            $table::getEntity()->createDbTable();
        }
    }

    public function UninstallDB()
    {
        $db = Application::getConnection();
        /** @var $table \Bitrix\Main\Entity\DataManager */
        foreach (array_reverse(self::$tables) as $table) {
            $db->dropTable($table::getTableName());
        }
    }

    public function InstallFiles()
    {
        $rSite = CSite::GetList($by = 'LID', $order = 'asc');
        while ($site = $rSite->GetNext()) {
            $rootPath = rtrim($site['ABS_DOC_ROOT'] ?: Application::getDocumentRoot(), DIRECTORY_SEPARATOR);
            $modulePath = rtrim(dirname(__DIR__), DIRECTORY_SEPARATOR);

            $folder = COption::GetOptionString(self::MODULE_ID, 'folder');
            $folderPath = $rootPath . $folder;

            foreach (self::$links as $path => $src) {
                $path = str_replace(['#root#', '#module#', '#folder#'], [$rootPath, $modulePath, $folder], $path);
                $src = str_replace(['#root#', '#module#', '#folder#'], [$rootPath, $modulePath, $folder], $src);
                $dir = dirname($path);
                if (strlen($dir)) {
                    mkdir($dir, BX_DIR_PERMISSIONS, true);
                }
                if (!is_link($path)){
                    if (!symlink($this->getRelative($path, $src), $path)) {
                        throw new \Bitrix\Main\SystemException("Can not create " . $path . " in " . $dir);
                    }
                }

            }

            CopyDirFiles(
                __DIR__ . "/admin",
                $rootPath . "/bitrix/admin",
                true
            );


            if (strpos($folderPath, $site['ABS_DOC_ROOT']) == 0) {
                UrlRewriter::add($site['LID'], [
                    'CONDITION' => '#^/' . $folder . '/#',
                    'RULE' => '',
                    'ID' => self::MODULE_ID . ':gateway',
                    'PATH' => '/' . $folder . '/index.php'
                ]);
            }
        }
    }

    function UninstallFiles()
    {
        $rSite = CSite::GetList($by = 'LID', $order = 'asc');
        while ($site = $rSite->GetNext()) {
            $rootPath = rtrim($site['ABS_DOC_ROOT'] ?: Application::getDocumentRoot(), DIRECTORY_SEPARATOR);;
            $modulePath = rtrim(dirname(__DIR__), DIRECTORY_SEPARATOR);

            $folder = COption::GetOptionString(self::MODULE_ID, 'folder');
            $folderPath = $rootPath . $folder;

            foreach (array_keys(self::$links) as $path) {
                $path = str_replace(['#root#', '#module#', '#folder#'], [$rootPath, $modulePath, $folder], $path);
                unlink($path);
            }


            if (strpos($folderPath, $site['ABS_DOC_ROOT']) == 0) {
                UrlRewriter::delete($site['LID'], [
                    'CONDITION' => '#^/' . $folder . '/#',
                    'ID' => self::MODULE_ID . ':gateway',
                    'PATH' => '/' . $folder . '/index.php'
                ]);
            }
        }

    }

    public function InstallTemplates()
    {
        $folder = COption::GetOptionString(self::MODULE_ID, 'folder');
        $folderPath = rtrim($_SERVER['DOCUMENT_ROOT'], '/') . '/' . $folder;

        $rSite = CSite::GetList($by = 'LID', $order = 'asc');
        $updater = new CSite();
        while ($site = $rSite->GetNext()) {
            if (strpos($folderPath, $site['ABS_DOC_ROOT']) == 0) {
                $arFields = [
                    'ACTIVE' => $site['ACTIVE'],
                    'SORT' => $site['SORT'],
                    'DEF' => $site['DEF'],
                    'NAME' => $site['NAME'],
                    'DIR' => $site['DIR'],
                    'SITE_NAME' => $site['SITE_NAME'],
                    'SERVER_NAME' => $site['SERVER_NAME'],
                    'EMAIL' => $site['EMAIL'],
                    'LANGUAGE_ID' => $site['LANGUAGE_ID'],
                    'DOC_ROOT' => $site['DOC_ROOT'],
                    'DOMAINS' => $site['DOMAINS'],
                    'CULTURE_ID' => $site['CULTURE_ID'],
                    'TEMPLATE' => []
                ];

                $rTemplate = CSite::GetTemplateList($site['LID']);
                $maxSort = 0;
                while ($template = $rTemplate->GetNext()) {
                    $arFields['TEMPLATE'][] = [
                        'TEMPLATE' => $template['TEMPLATE'],
                        'SORT' => $template['SORT'],
                        'CONDITION' => $template['CONDITION']
                    ];
                    if ((int)$template['SORT'] > $maxSort) {
                        $maxSort = (int)$template['SORT'];
                    }
                }
                $arFields['TEMPLATE'][] = [
                    'TEMPLATE' => 'payments_vw',
                    'SORT' => (string)($maxSort + 1),
                    'CONDITION' => 'CSite::InDir(\'/' . $folder . '\')'
                ];

                $updater->Update($site['LID'], $arFields);
            }
        }
    }

    public function UninstallTemplates()
    {
        $templates = $this->getTemplatesList();
        $folder = COption::GetOptionString(self::MODULE_ID, 'folder');
        $folderPath = rtrim($_SERVER['DOCUMENT_ROOT'], '/') . '/' . $folder;

        $rSite = CSite::GetList($by = 'LID', $order = 'asc');
        $updater = new CSite();
        while ($site = $rSite->GetNext()) {
            if (strpos($folderPath, $site['ABS_DOC_ROOT']) == 0) {
                $arFields = [
                    'ACTIVE' => $site['ACTIVE'],
                    'SORT' => $site['SORT'],
                    'DEF' => $site['DEF'],
                    'NAME' => $site['NAME'],
                    'DIR' => $site['DIR'],
                    'SITE_NAME' => $site['SITE_NAME'],
                    'SERVER_NAME' => $site['SERVER_NAME'],
                    'EMAIL' => $site['EMAIL'],
                    'LANGUAGE_ID' => $site['LANGUAGE_ID'],
                    'DOC_ROOT' => $site['DOC_ROOT'],
                    'DOMAINS' => $site['DOMAINS'],
                    'CULTURE_ID' => $site['CULTURE_ID'],
                    'TEMPLATE' => []
                ];

                $rTemplate = CSite::GetTemplateList($site['LID']);
                $maxSort = 0;
                while ($template = $rTemplate->GetNext()) {
                    if (!in_array($template['TEMPLATE'], $templates)) {
                        $arFields['TEMPLATE'][] = [
                            'TEMPLATE' => $template['TEMPLATE'],
                            'SORT' => $template['SORT'],
                            'CONDITION' => $template['CONDITION']
                        ];
                        if ((int)$template['SORT'] > $maxSort) {
                            $maxSort = (int)$template['SORT'];
                        }
                    }
                }

                $updater->Update($site['LID'], $arFields);
            }
        }
    }

    public function InstallAgents()
    {
    }

    public function UninstallAgents()
    {
    }

    private function getTemplatesList()
    {
        $path = dirname(__DIR__) . '/templates/';
        $dir = dir($path);
        $templates = [];
        while ($item = $dir->read()) {
            if (is_dir($path . $item) && !in_array($item, ['.', '..'])) {
                $templates[] = $item;
            }
        }
        $dir->close();
        return $templates;
    }

    private function autoload($class)
    {
        $class = ltrim($class, '\\');
        if (strpos($class, 'Kodix\\Payments\\') == 0) {
            $path = sprintf(
                '%slib/%s.php',
                rtrim(dirname(__DIR__), DIRECTORY_SEPARATOR) . '/',
                implode('/', array_slice(explode('\\', $class), 2))
            );
            if (is_readable($path)) {
                require_once $path;
            }
        }
    }

    private function getRelative($path, $to)
    {
        $a = explode(DIRECTORY_SEPARATOR, $path);
        $b = explode(DIRECTORY_SEPARATOR, $to);

        while ($a[0] == $b[0]) {
            array_shift($a);
            array_shift($b);
        }

        for ($i = 0; $i < count($a) - 1; $i++) {
            array_unshift($b, '..');
        }

        switch ($b[0]) {
            case '..':
            case '':
                break;
            default:
                array_unshift($b, '.');
        }

        return implode(DIRECTORY_SEPARATOR, $b);
    }
}