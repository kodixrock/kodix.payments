<?php

global $APPLICATION, $ERRORS;

$arErrors = $ERRORS;

$publicKey = COption::GetOptionString(kodix_payments::MODULE_ID, 'public_key');
$privateKey = COption::GetOptionString(kodix_payments::MODULE_ID, 'private_key');
$folder = COption::GetOptionString(kodix_payments::MODULE_ID, 'folder', '/payments/');

if (!strlen($privateKey) || !strlen($publicKey)) {
    list($publicKey, $privateKey) = \Kodix\Payments\Models\AccountModel::getRSAKeys();
}

$arResult = [
    'folder' => !is_null($_POST['folder']) ? $_POST['folder'] : $folder,
    'public_key' => !is_null($_POST['public_key']) ? $_POST['public_key'] : $publicKey,
    'private_key' => !is_null($_POST['private_key']) ? $_POST['private_key'] : $privateKey,
    'login' => $_POST['login'],
];

$arMenu = [
    [
        'TEXT' => GetMessage('KODIX_PAYMENTS_INSTALL_BACK'),
        'LINK' => 'partner_modules.php?lang=' . LANGUAGE_ID,
        'ICON' => 'btn_list',
        'TITLE' => '',
    ],
];
$context = new CAdminContextMenu($arMenu);
$context->Show();

if (!empty($arErrors)) {
    $message = new CAdminMessage(implode("<br>", $arErrors));
    echo $message->Show();
}

$arTabs = [
    [
        'DIV' => 'folder',
        'TAB' => GetMessage('KODIX_PAYMENTS_INSTALL_FOLDER'),
        'TITLE' => GetMessage('KODIX_PAYMENTS_INSTALL_FOLDER_TITLE'),
    ],
    [
        'DIV' => 'account',
        'TAB' => GetMessage('KODIX_PAYMENTS_INSTALL_ACCOUNT'),
        'TITLE' => GetMessage('KODIX_PAYMENTS_INSTALL_ACCOUNT_TITLE'),
    ],
    [
        'DIV' => 'keys',
        'TAB' => GetMessage('KODIX_PAYMENTS_INSTALL_KEYS'),
        'TITLE' => GetMessage('KODIX_PAYMENTS_INSTALL_KEYS_TITLE'),
    ],
];
$form = new CAdminForm('folder_form', $arTabs);

$form->Begin(['FORM_ACTION' => sprintf(
    '%s?id=%s&lang=%s&install=Y&sessid=%s',
    $APPLICATION->GetCurPage(),
    kodix_payments::MODULE_ID,
    LANG,
    bitrix_sessid()
)]);

$form->BeginNextFormTab();
$form->AddEditField(
    'folder',
    GetMessage('KODIX_PAYMENTS_INSTALL_FOLDER_PATH'),
    true,
    ['size' => 40, 'maxlength' => 255],
    htmlspecialchars($arResult['folder'])
);

$form->BeginNextFormTab();
$form->BeginCustomField('message','');
echo '<div style="margin: 0 auto 15px 0;text-align: center;">'.GetMessage('KODIX_PAYMENTS_INSTALL_ACCOUNT_MESSAGE').'</div>';
$form->EndCustomField('message');
$form->AddEditField(
    'login',
    GetMessage('KODIX_PAYMENTS_INSTALL_ACCOUNT_LOGIN'),
    true,
    ['size' => 40, 'maxlength' => 255],
    htmlspecialchars($arResult['login'])
);
$form->AddEditField(
    'secret',
    GetMessage('KODIX_PAYMENTS_INSTALL_ACCOUNT_SECRET'),
    true,
    ['size' => 40, 'maxlength' => 255]
);
$form->BeginNextFormTab();
$form->AddTextField(
    'public_key',
    GetMessage('KODIX_PAYMENTS_INSTALL_KEYS_PUBLIC_KEY'),
    htmlspecialchars($arResult['public_key']),
    ['cols' => 75, 'rows' => 10],
    true
);
$form->AddTextField(
    'private_key',
    GetMessage('KODIX_PAYMENTS_INSTALL_KEYS_PRIVATE_KEY'),
    htmlspecialchars($arResult['private_key']),
    ['cols' => 75, 'rows' => 29],
    true
);

$form->sButtonsContent = sprintf(
    '<input type="submit" name="apply" value="%s" title="%s" class="adm-btn-save">',
    htmlspecialchars(GetMessage('KODIX_PAYMENTS_INSTALL_BTN')),
    htmlspecialchars(GetMessage('KODIX_PAYMENTS_INSTALL_BTN_TITLE'))
);
$form->Show();
