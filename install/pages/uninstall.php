<?php

global $APPLICATION, $ERRORS;

$arErrors = $ERRORS;

$arMenu = [
    [
        'TEXT' => GetMessage('KODIX_PAYMENTS_UNINSTALL_BACK'),
        'LINK' => 'partner_modules.php?lang=' . LANGUAGE_ID,
        'ICON' => 'btn_list',
        'TITLE' => '',
    ],
];
$context = new CAdminContextMenu($arMenu);
$context->Show();

if (!empty($arErrors)) {
    $message = new CAdminMessage(implode("<br>", $arErrors));
    echo $message->Show();
}

$arTabs = [
    [
        'DIV' => 'remove',
        'TAB' => GetMessage('KODIX_PAYMENTS_UNINSTALL_TAB'),
        'TITLE' => GetMessage('KODIX_PAYMENTS_UNINSTALL_TAB_TITLE'),
    ],
];
$form = new CAdminForm('folder_form', $arTabs);

$form->Begin(['FORM_ACTION' => sprintf(
    '%s/id=%s&lang=%s&uninstall=Y&sessid=%s',
    $APPLICATION->GetCurPage(),
    kodix_payments::MODULE_ID,
    bitrix_sessid()
)]);

$form->BeginNextFormTab();
$form->BeginCustomField('message','');
echo GetMessage('KODIX_PAYMENTS_UNINSTALL_MESSAGE');
$form->EndCustomField('message');

$form->sButtonsContent = sprintf(
    '<input type="submit" name="apply" value="%s" title="%s" class="adm-btn-save">',
    htmlspecialchars(GetMessage('KODIX_PAYMENTS_UNINSTALL_BTN')),
    htmlspecialchars(GetMessage('KODIX_PAYMENTS_UNINSTALL_BTN_TITLE'))
);
$form->Show();
