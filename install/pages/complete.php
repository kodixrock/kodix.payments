<?php

global $APPLICATION, $ACCOUNT;

$folder = COption::GetOptionString('kodix.payments', 'folder');
$sitePrefix = sprintf(
    '%s://%s/',
    $request->isHttps() ? 'https' : 'http',
    $request->getHttpHost()
);
$prefix = $sitePrefix . trim($folder, DIRECTORY_SEPARATOR) . '/';
$tokenUrl = $prefix . 'auth';
$newOrderUrl = $prefix . 'order';
$getOrderUrl = $prefix . 'order/#id#';

$arTabs = [
    [
        'DIV' => 'complete',
        'TAB' => GetMessage('KODIX_PAYMENTS_INSTALL_COMPLETE'),
        'TITLE' => GetMessage('KODIX_PAYMENTS_INSTALL_COMPLETE_TITLE'),
    ],
];
$form = new CAdminForm('folder_form', $arTabs);
$form->Begin(['FORM_ACTION' => sprintf(
    '%s?lang=%s&sessid=%s',
    $APPLICATION->GetCurPage(),
    LANG,
    bitrix_sessid()
)]);
$form->BeginNextFormTab();
$form->AddSection('CONNECT', GetMessage('KODIX_PAYMENTS_INSTALL_COMPLETE_MESSAGE'));
$form->AddViewField(
    'AUTH_URL',
    GetMessage('KODIX_PAYMENTS_INSTALL_COMPLETE_TOKEN_URL'),
    $tokenUrl
);
$form->AddViewField(
    'NEW_ORDER_URL',
    GetMessage('KODIX_PAYMENTS_INSTALL_COMPLETE_NEW_ORDER_URL'),
    $newOrderUrl
);
$form->AddViewField(
    'GET_ORDER_URL',
    GetMessage('KODIX_PAYMENTS_INSTALL_COMPLETE_GET_ORDER_URL'),
    $getOrderUrl
);
if (is_array($ACCOUNT)) {
    $form->AddViewField(
        'LOGIN',
        GetMessage('KODIX_PAYMENTS_INSTALL_COMPLETE_LOGIN'),
        $ACCOUNT['login']
    );
    $form->AddViewField(
        'SECRET',
        GetMessage('KODIX_PAYMENTS_INSTALL_COMPLETE_SECRET'),
        $ACCOUNT['secret']
    );
}
$form->AddSection('STEPS', GetMessage('KODIX_PAYMENTS_INSTALL_COMPLETE_STEPS'));
$form->AddViewField(
    'MERCHANT',
    GetMessage('KODIX_PAYMENTS_INSTALL_COMPLETE_MERCHANT'),
    sprintf(
        '<a href="%s">%s</a>',
        $sitePrefix . '/bitrix/admin/kodix_payments_merchant_edit.php?lang=' . LANGUAGE_ID,
        'kodix_payments_merchant_edit.php'
    )
);
if (is_null($ACCOUNT)) {
    $form->AddViewField(
        'ACCOUNT',
        GetMessage('KODIX_PAYMENTS_INSTALL_COMPLETE_ACCOUNT'),
        sprintf(
            '<a href="%s">%s</a>',
            $sitePrefix . '/bitrix/admin/kodix_payments_account_edit.php?lang=' . LANGUAGE_ID,
            'kodix_payments_account_edit.php'
        )
    );
}
$form->Show();