<?php

return [
    'MODULE_ID' => 'kodix.payments',
    'MODULE_NAME' => 'Kodix Online Payments',
    'MODULE_VERSION' => '1.0.0',
    'MODULE_VERSION_DATE' => '2018-09-25 16:00:00',
    'PARTNER_NAME' => 'Kodix LLC',
    'PARTNER_URI' => 'https://www.kodix.ru',
];