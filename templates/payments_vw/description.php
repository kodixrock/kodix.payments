<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

\Bitrix\Main\Localization\Loc::loadMessages(__FILE__);
$arTemplate = Array("NAME" => GetMessage('T_DEFAULT_DESC_NAME'), "DESCRIPTION" => GetMessage('T_DEFAULT_DESC_DESC'));