<!DOCTYPE html>
<html>
<head>
    <title><? $APPLICATION->ShowTitle() ?></title>
    <meta charset="UTF-8"/>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport"/>
    <? $APPLICATION->ShowHead() ?>
</head>
<body>
<div class="wrapper">
    <div class="vw_header">
        <h1 class="vw_header__title"><?= GetMessage('TITLE') ?></h1>
        <div class="vw_header__desc"><?= GetMessage('DESCRIPTION') ?></div>
    </div>
    <div class="vw_main">
        <div class="page-container">
