<?php
include($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');

use Bitrix\Main\Loader;

if (Loader::includeModule('kodix.payments')) {
    $folder = COption::GetOptionString('kodix.payments', 'folder');
    $APPLICATION->IncludeComponent(
        'kodix.payments:gateway',
        '',
        ['SEF_FOLDER' => '/' . $folder . '/']
    );
}

include($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');
