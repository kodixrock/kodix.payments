<?php

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Loader;
use Kodix\Payments\Models\AccountModel;

define("ADMIN_MODULE_NAME", "kodix.payments");

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php');

Loc::loadLanguageFile(__FILE__);


$APPLICATION->SetTitle(GetMessage(strlen($_REQUEST['ID']) ? 'KODIX_PAYMENTS_ACCOUNT_EDIT' : 'KODIX_PAYMENTS_ACCOUNT_NEW'));

if (!Loader::includeModule(ADMIN_MODULE_NAME)) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_after.php');
    ShowError(GetMessage('KODIX_PAYMENTS_ERR_NOT_INSTALLED'));
    require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/epilog_admin.php');
    die();
}

$MODULE_RIGHT = $APPLICATION->GetGroupRight(ADMIN_MODULE_NAME);

if ($MODULE_RIGHT == 'D') {
    $APPLICATION->AuthForm(GetMessage('ACCESS_DENIED'));
}

$table = AccountModel::getTable();

$arErrors = [];
$arResult = [];
$account = false;

if (strlen($_REQUEST['ID'])) {
    $account = $table::getById($_REQUEST['ID'])->fetch();
    if (is_array($account)) {
        $arResult = $account;
    }
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    foreach ($table::getMap() as $name => $field) {
        $value = strlen($_REQUEST[$name]) ? $_REQUEST[$name] : $account[$name];
        switch (true) {
            case $name == 'SECRET':
                if (strlen($_REQUEST[$name])) {
                    $value = $table::getHash($_REQUEST[$name]);
                }
                break;
            case $field['data_type'] == 'boolean':
                $value = in_array($_REQUEST[$name], ['Y', 'y', 1, true]) ? 'Y' : 'N';
                break;
        }
        if(!is_null($value)) {
            $arResult[$name] = $value;
        }
    }
    if (is_array($account)) {
        $result = $table::update($account['ID'], $arResult);
    } else {
        $result = $table::add($arResult);
    }

    if ($result->isSuccess()) {
        LocalRedirect(sprintf(
            'kodix_payments_account_edit.php?ID=%s&lang=%s',
            $result->getId(),
            LANGUAGE_ID
        ));
    } else {
        $arErrors = $result->getErrorMessages();
    }
}

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

$arMenu = [
    [
        'TEXT' => GetMessage('KODIX_PAYMENTS_ACCOUNT_LIST'),
        'LINK' => 'kodix_payments_account_list.php?lang=' . LANGUAGE_ID,
        'ICON' => 'btn_list',
        'TITLE' => '',
    ],
];

$context = new CAdminContextMenu($arMenu);
$context->Show();

if (!empty($arErrors)) {
    $message = new CAdminMessage(implode("<br>", $arErrors));
    echo $message->Show();
}

$arTabs = [
    [
        'DIV' => 'folder',
        'TAB' => GetMessage('KODIX_PAYMENTS_ACCOUNT'),
        'TITLE' => GetMessage(strlen($_REQUEST['ID']) ? 'KODIX_PAYMENTS_ACCOUNT_EDIT' : 'KODIX_PAYMENTS_ACCOUNT_NEW'),
    ],
];
$form = new CAdminForm('folder_form', $arTabs);
$form->Begin(['FORM_ACTION' => sprintf(
    '%s?ID=%s&lang=%s',
    $APPLICATION->GetCurPage(),
    $arResult['ID'],
    LANGUAGE_ID
)]);
$form->BeginNextFormTab();
if (strlen($arResult['ID'])) {
    $form->AddViewField(
        'ID',
        GetMessage('KODIX_PAYMENTS_ACCOUNT_ID'),
        $arResult['ID']
    );
    $form->AddViewField(
        'CREATED_AT',
        GetMessage('KODIX_PAYMENTS_ACCOUNT_CREATED_AT'),
        $arResult['CREATED_AT']
    );
    $form->AddViewField(
        'UPDATED_AT',
        GetMessage('KODIX_PAYMENTS_ACCOUNT_UPDATED_AT'),
        $arResult['UPDATED_AT']
    );
    $form->AddViewField(
        'LOGINED_AT',
        GetMessage('KODIX_PAYMENTS_ACCOUNT_LOGINED_AT'),
        $arResult['LOGINED_AT']
    );
}
$form->AddEditField(
    'LOGIN',
    GetMessage('KODIX_PAYMENTS_ACCOUNT_LOGIN'),
    true,
    ['size' => 40, 'maxlength' => 255],
    htmlspecialchars($arResult['LOGIN'])
);
$form->AddEditField(
    'SECRET',
    GetMessage('KODIX_PAYMENTS_ACCOUNT_SECRET'),
    true,
    ['size' => 40, 'maxlength' => 255]
);
$form->AddCheckBoxField(
    'ACTIVE',
    GetMessage('KODIX_PAYMENTS_ACCOUNT_ACTIVE'),
    true,
    'Y',
    $arResult['ACTIVE'] == 'Y'
);
$form->Buttons(['back_url' => 'kodix_payments_currency_list.php?lang=' . LANGUAGE_ID]);
$form->Show();

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");