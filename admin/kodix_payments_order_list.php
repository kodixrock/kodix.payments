<?php

use Bitrix\Main\Application;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Loader;
use Kodix\Payments\Tables\OrderTable;

define("ADMIN_MODULE_NAME", "kodix.payments");

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php');

Loc::loadLanguageFile(__FILE__);

$APPLICATION->SetTitle(GetMessage('KODIX_PAYMENTS_ORDER_LIST'));

if (!Loader::includeModule(ADMIN_MODULE_NAME)) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_after.php');
    ShowError(GetMessage('KODIX_PAYMENTS_ERR_NOT_INSTALLED'));
    require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/epilog_admin.php');
    die();
}

$MODULE_RIGHT = $APPLICATION->GetGroupRight(ADMIN_MODULE_NAME);

if ($MODULE_RIGHT == 'D') {
    $APPLICATION->AuthForm(GetMessage('ACCESS_DENIED'));
}

$oSort = new CAdminSorting(OrderTable::getTableName(), 'CREATED_AT', 'DESC');
$arOrder = [$oSort->getField() => $oSort->getOrder()];

$list = new CAdminList(OrderTable::getTableName(), $oSort);
$list->ShowChain($list->CreateChain());

$list->AddAdminContextMenu($aContext);

$arHeaders = [];
$defaults = ['ID', 'EXTERNAL_ID', 'PAYMENT_ID', 'STATUS', 'AMOUNT', 'CURRENCY_ID', 'CREATED_AT', 'UPDATED_AT'];
foreach (OrderTable::getMap() as $name => $field) {
    $title = GetMessage('KODIX_PAYMENTS_ORDER_' . $name);
    $arHeaders[] = [
        'id' => $name,
        'content' => strlen($title) ? $title : $field['title'],
        'sort' => $name,
        'default' => in_array($name, $defaults),
    ];
}
$list->AddHeaders($arHeaders);

$query = new CAdminResult(OrderTable::getList([
    'order' => $arOrder,
]), OrderTable::getTableName());
$query->NavStart();

$list->NavText($query->GetNavPrint(GetMessage('PAGES')));

while ($order = $query->NavNext()) {
    $row = $list->AddRow($order['ID'], $order);

    $editLink = sprintf(
        'kodix_payments_order_view.php?ID=%s&lang=%s',
        $order['ID'],
        LANGUAGE_ID
    );

    $row->AddViewField(
        'ID',
        sprintf(
            '<a href="%s">%s</a>',
            $editLink,
            htmlspecialchars($order['ID'])
        )
    );
    $row->AddActions([
        [
            'TEXT' => GetMessage('KODIX_PAYMENTS_ORDER_VIEW'),
            'ICON' => 'view',
            'ACTION' => $list->ActionRedirect($editLink)
        ]
    ]);
}

$list->AddFooter([]);

$list->CheckListMode();

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_after.php');

$list->DisplayList();

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/epilog_admin.php');