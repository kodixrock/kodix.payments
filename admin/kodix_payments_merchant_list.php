<?php

use Bitrix\Main\Application;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Loader;
use Kodix\Payments\Tables\MerchantTable;

define("ADMIN_MODULE_NAME", "kodix.payments");

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php');

Loc::loadLanguageFile(__FILE__);

$APPLICATION->SetTitle(GetMessage('KODIX_PAYMENTS_MERCHANT_LIST'));

if (!Loader::includeModule(ADMIN_MODULE_NAME)) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_after.php');
    ShowError(GetMessage('KODIX_PAYMENTS_ERR_NOT_INSTALLED'));
    require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/epilog_admin.php');
    die();
}

$MODULE_RIGHT = $APPLICATION->GetGroupRight(ADMIN_MODULE_NAME);

if ($MODULE_RIGHT == 'D') {
    $APPLICATION->AuthForm(GetMessage('ACCESS_DENIED'));
}

$action = strlen($_REQUEST['action']) ? $_REQUEST['action'] : strlen($_REQUEST['action_button']) ? $_REQUEST['action_button'] : '';
if ($MODULE_RIGHT == 'W' && $action == 'delete') {
    $db = Application::getConnection();
    $db->startTransaction();
    foreach ((array)$_REQUEST['ID'] as $id) {
        MerchantTable::delete($id);
    }
    $db->commitTransaction();
}

$oSort = new CAdminSorting(MerchantTable::getTableName(), 'SORT', 'ASC');
$arOrder = [$oSort->getField() => $oSort->getOrder()];

$list = new CAdminList(MerchantTable::getTableName(), $oSort);
$list->ShowChain($list->CreateChain());

$aContext = [
    [
        'TEXT' => GetMessage('KODIX_PAYMENTS_MERCHANT_NEW'),
        'TITLE' => GetMessage('KODIX_PAYMENTS_MERCHANT_NEW'),
        'ICON' => 'btn_new',
        'LINK' => 'kodix_payments_merchant_edit.php?lang=' . LANGUAGE_ID
    ]
];
$list->AddAdminContextMenu($aContext);

$arHeaders = [];
$defaults = ['ID', 'NAME', 'PROVIDER', 'SORT', 'ACTIVE', 'CREATED_AT', 'UPDATED_AT'];
foreach (MerchantTable::getMap() as $name => $field) {
    $title = GetMessage('KODIX_PAYMENTS_MERCHANT_' . $name);
    $arHeaders[] = [
        'id' => $name,
        'content' => strlen($title) ? $title : $field['title'],
        'sort' => $name,
        'default' => in_array($name, $defaults),
    ];
}
$list->AddHeaders($arHeaders);

$query = new CAdminResult(MerchantTable::getList([
    'order' => $arOrder,
]), MerchantTable::getTableName());
$query->NavStart();

$list->NavText($query->GetNavPrint(GetMessage('PAGES')));

while ($merchant = $query->Fetch()) {
    $row = $list->AddRow($merchant['ID'], $merchant);

    $editLink = sprintf(
        'kodix_payments_merchant_edit.php?ID=%s&lang=%s',
        $merchant['ID'],
        LANGUAGE_ID
    );

    if ($MODULE_RIGHT == 'W') {
        $row->AddViewField(
            'NAME',
            sprintf(
                '<a href="%s">%s</a>',
                $editLink,
                htmlspecialchars($merchant['NAME'])
            )
        );
        $row->AddActions([
            [
                'TEXT' => GetMessage('KODIX_PAYMENTS_MERCHANT_EDIT'),
                'ICON' => 'edit',
                'ACTION' => $list->ActionRedirect($editLink)
            ],
            [
                'TEXT' => GetMessage('KODIX_PAYMENTS_MERCHANT_REMOVE'),
                'ICON' => 'delete',
                'ACTION' => 'if(confirm("' . GetMessage('KODIX_PAYMENTS_MERCHANT_REMOVE_CONF') . '")) ' .
                    $list->ActionDoGroup($merchant['ID'], 'delete')
            ]
        ]);
    }
}


if ($MODULE_RIGHT == 'W') {
    $list->AddGroupActionTable(
        [
            'delete' => true,
        ]
    );
}

$list->AddFooter([
    [
        'title' => GetMessage('MAIN_ADMIN_LIST_SELECTED'),
        'value' => 0
    ],
    [
        'counter' => true,
        'title' => GetMessage('MAIN_ADMIN_LIST_CHECKED'),
        'value' => 0
    ]
]);

$list->CheckListMode();

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_after.php');

$list->DisplayList();

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/epilog_admin.php');