<?php

$MESS['KODIX_PAYMENTS_MERCHANT_LIST'] = 'Список профилей';
$MESS['KODIX_PAYMENTS_MERCHANT_NEW'] = 'Новый профиль';
$MESS['KODIX_PAYMENTS_MERCHANT_EDIT'] = 'Редактирование профиля';
$MESS['KODIX_PAYMENTS_MERCHANT_REMOVE'] = 'Удалить профиль';
$MESS['KODIX_PAYMENTS_MERCHANT_REMOVE_CONF'] = 'Вы действительно хотите удалить профиль?';

$MESS['KODIX_PAYMENTS_MERCHANT_PROFILE'] = 'Профиль';
$MESS['KODIX_PAYMENTS_MERCHANT_PROFILE_TITLE'] = 'Профиль';
$MESS['KODIX_PAYMENTS_MERCHANT_PROVIDER'] = 'Провайдер';
$MESS['KODIX_PAYMENTS_MERCHANT_PROVIDER_TITLE'] = 'Настройки провайдера';

$MESS['KODIX_PAYMENTS_MERCHANT_ID'] = 'ID';
$MESS['KODIX_PAYMENTS_MERCHANT_NAME'] = 'Наименование';
$MESS['KODIX_PAYMENTS_MERCHANT_IMAGE_ID'] = 'Изображение';
$MESS['KODIX_PAYMENTS_MERCHANT_IMAGE'] = 'Изображение';
$MESS['KODIX_PAYMENTS_MERCHANT_ACTIVE'] = 'Активность';
$MESS['KODIX_PAYMENTS_MERCHANT_PROVIDER'] = 'Провайдер';
$MESS['KODIX_PAYMENTS_MERCHANT_CONFIG'] = 'Настройки';
$MESS['KODIX_PAYMENTS_MERCHANT_SORT'] = 'Сортировка';
$MESS['KODIX_PAYMENTS_MERCHANT_CREATED_AT'] = 'Создан';
$MESS['KODIX_PAYMENTS_MERCHANT_UPDATED_AT'] = 'Обновлен';

$MESS['KODIX_PAYMENTS_MERCHANT_PROPS_URL'] = 'Url';
$MESS['KODIX_PAYMENTS_MERCHANT_PROPS_LOGIN'] = 'Логин';
$MESS['KODIX_PAYMENTS_MERCHANT_PROPS_PASSWORD'] = 'Пароль';
$MESS['KODIX_PAYMENTS_MERCHANT_PROPS_TWO_STAGE'] = 'Двухстадийных платеж';

$MESS['KODIX_PAYMENTS_ERR_NOT_INSTALLED'] = 'Модуль не установлен';
$MESS['KODIX_PAYMENTS_ERR_PROVIDER'] = 'Неизвестный провайдер';
$MESS['KODIX_PAYMENTS_ERR_PROVIDER_CONFIG'] = 'Не заполнено обязательное поле #name#';
