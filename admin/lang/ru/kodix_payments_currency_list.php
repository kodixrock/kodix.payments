<?php

$MESS['KODIX_PAYMENTS_CURRENCY_LIST'] = 'Список валют';
$MESS['KODIX_PAYMENTS_CURRENCY_NEW'] = 'Создать валюту';
$MESS['KODIX_PAYMENTS_CURRENCY_EDIT'] = 'Изменить валюту';
$MESS['KODIX_PAYMENTS_CURRENCY_REMOVE'] = 'Удалить валюту';
$MESS['KODIX_PAYMENTS_CURRENCY_REMOVE_CONF'] = 'Вы действительно хотите удалть валюту?';

$MESS['KODIX_PAYMENTS_CURRENCY_ID'] = 'Код';
$MESS['KODIX_PAYMENTS_CURRENCY_NAME'] = 'Наименование';
$MESS['KODIX_PAYMENTS_CURRENCY_SHORT_NAME'] = 'Короткое обозначение';
$MESS['KODIX_PAYMENTS_CURRENCY_ISO'] = 'ISO';
$MESS['KODIX_PAYMENTS_CURRENCY_NOMINAL'] = 'Номинал';
$MESS['KODIX_PAYMENTS_CURRENCY_VALUE'] = 'Стоимость';
$MESS['KODIX_PAYMENTS_CURRENCY_CREATED_AT'] = 'Создана';
$MESS['KODIX_PAYMENTS_CURRENCY_UPDATED_AT'] = 'Обновлена';

$MESS['KODIX_PAYMENTS_ERR_NOT_INSTALLED'] = 'Модуль не установлен';
