<?php

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Loader;
use Kodix\Payments\Tables\CurrencyTable;

define("ADMIN_MODULE_NAME", "kodix.payments");

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php');

Loc::loadLanguageFile(__FILE__);

$APPLICATION->SetTitle(GetMessage(strlen($_REQUEST['ID']) ? 'KODIX_PAYMENTS_CURRENCY_EDIT' : 'KODIX_PAYMENTS_CURRENCY_NEW'));

if (!Loader::includeModule(ADMIN_MODULE_NAME)) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_after.php');
    ShowError(GetMessage('KODIX_PAYMENTS_ERR_NOT_INSTALLED'));
    require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/epilog_admin.php');
    die();
}

$MODULE_RIGHT = $APPLICATION->GetGroupRight(ADMIN_MODULE_NAME);

if ($MODULE_RIGHT == 'D') {
    $APPLICATION->AuthForm(GetMessage('ACCESS_DENIED'));
}

$arErrors = [];
$arResult = [];
$currency = false;

if (strlen($_REQUEST['ID'])) {
    $currency = CurrencyTable::getById($_REQUEST['ID'])->fetch();
    if (is_array($currency)) {
        $arResult = $currency;
    }
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    foreach (CurrencyTable::getMap() as $name => $field) {
        $arResult[$name] = $_REQUEST[$name];
    }
    if (is_array($currency)) {
        $result = CurrencyTable::update($currency['ID'], $arResult);
    } else {
        $result = CurrencyTable::add($arResult);
    }

    if ($result->isSuccess()) {
        LocalRedirect(sprintf(
            'kodix_payments_currency_edit.php?ID=%s&lang=%s',
            $result->getID(),
            LANGUAGE_ID
        ));
    } else {
        $arErrors = $result->getErrorMessages();
    }
}

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

$arMenu = [
    [
        'TEXT' => GetMessage('KODIX_PAYMENTS_CURRENCY_LIST'),
        'LINK' => 'kodix_payments_currency_list.php?lang=' . LANGUAGE_ID,
        'ICON' => 'btn_list',
        'TITLE' => '',
    ],
];
$context = new CAdminContextMenu($arMenu);
$context->Show();

if (!empty($arErrors)) {
    $message = new CAdminMessage(implode("<br>", $arErrors));
    echo $message->Show();
}

$arTabs = [
    [
        'DIV' => 'folder',
        'TAB' => GetMessage('KODIX_PAYMENTS_CURRENCY'),
        'TITLE' => GetMessage(strlen($_REQUEST['ID']) ? 'KODIX_PAYMENTS_CURRENCY_EDIT' : 'KODIX_PAYMENTS_CURRENCY_NEW'),
    ],
];
$form = new CAdminForm('folder_form', $arTabs);
$form->Begin(['FORM_ACTION' => sprintf(
    '%s?ID=%s&lang=%s',
    $APPLICATION->GetCurPage(),
    $arResult['ID'],
    LANGUAGE_ID
)]);
$form->BeginNextFormTab();
if (strlen($arResult['ID'])) {
    $form->AddViewField(
        'CREATED_AT',
        GetMessage('KODIX_PAYMENTS_CURRENCY_CREATED_AT'),
        $arResult['CREATED_AT']
    );
    $form->AddViewField(
        'UPDATED_AT',
        GetMessage('KODIX_PAYMENTS_CURRENCY_UPDATED_AT'),
        $arResult['UPDATED_AT']
    );
}
$form->AddEditField(
    'ID',
    GetMessage('KODIX_PAYMENTS_CURRENCY_ID'),
    true,
    ['size' => 40, 'maxlength' => 5],
    htmlspecialchars($arResult['ID'])
);
$form->AddEditField(
    'NAME',
    GetMessage('KODIX_PAYMENTS_CURRENCY_NAME'),
    true,
    ['size' => 40, 'maxlength' => 255],
    htmlspecialchars($arResult['NAME'])
);
$form->AddEditField(
    'SHORT_NAME',
    GetMessage('KODIX_PAYMENTS_CURRENCY_SHORT_NAME'),
    false,
    ['size' => 40, 'maxlength' => 255],
    htmlspecialchars($arResult['SHORT_NAME'])
);
$form->AddEditField(
    'ISO',
    GetMessage('KODIX_PAYMENTS_CURRENCY_ISO'),
    true,
    ['size' => 40, 'maxlength' => 255],
    htmlspecialchars($arResult['ISO'])
);
$form->AddEditField(
    'NOMINAL',
    GetMessage('KODIX_PAYMENTS_CURRENCY_NOMINAL'),
    true,
    ['size' => 40, 'maxlength' => 255],
    htmlspecialchars($arResult['NOMINAL'])
);
$form->AddEditField(
    'VALUE',
    GetMessage('KODIX_PAYMENTS_CURRENCY_VALUE'),
    true,
    ['size' => 40, 'maxlength' => 255],
    htmlspecialchars($arResult['NOMINAL'])
);
$form->Buttons(['back_url' => 'kodix_payments_currency_list.php?lang=' . LANGUAGE_ID]);
$form->Show();

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");