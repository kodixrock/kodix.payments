<?php

use Bitrix\Main\Localization\Loc;

if ($APPLICATION->GetGroupRight('kodix.payments') != 'D') {

    Loc::loadLanguageFile(__FILE__);

    switch (true) {
        case IsModuleInstalled('kmodule'):
            $parentMenu = "global_menu_kmodule";
            break;
        case IsModuleInstalled('kodix.main'):
            $parentMenu = "global_menu_kodix";
            break;
        default:
            $parentMenu = "global_menu_content";
            break;
    }

    return [
        'parent_menu' => $parentMenu,
        'section' => 'kodix_payments',
        'sort' => 1000,
        'text' => GetMessage('KODIX_PAYMENTS'),
        'title' => GetMessage('KODIX_PAYMENTS'),
        'icon' => null,
        'page_icon' => 'sys_page_icon',
        'items_id' => 'kodix_payments',
        'items' => [
            [
                'text' => GetMessage('KODIX_PAYMENTS_ORDER'),
                'title' => GetMessage('KODIX_PAYMENTS_ORDER'),
                'url' => 'kodix_payments_order_list.php',
                'more_url' => [
                    'kodix_payments_order_list.php',
                    'kodix_payments_order_view.php',
                ]
            ],
            [
                'text' => GetMessage('KODIX_PAYMENTS_ACCOUNT'),
                'title' => GetMessage('KODIX_PAYMENTS_ACCOUNT'),
                'url' => 'kodix_payments_account_list.php',
                'more_url' => [
                    'kodix_payments_account_list.php',
                    'kodix_payments_account_edit.php',
                ]
            ],
            [
                'text' => GetMessage('KODIX_PAYMENTS_MERCHANT'),
                'title' => GetMessage('KODIX_PAYMENTS_MERCHANT'),
                'url' => 'kodix_payments_merchant_list.php',
                'more_url' => [
                    'kodix_payments_merchant_list.php',
                    'kodix_payments_merchant_edit.php',
                ]
            ],
            [
                'text' => GetMessage('KODIX_PAYMENTS_CURRENCY'),
                'title' => GetMessage('KODIX_PAYMENTS_CURRENCY'),
                'url' => 'kodix_payments_currency_list.php',
                'more_url' => [
                    'kodix_payments_currency_list.php',
                    'kodix_payments_currency_edit.php',
                ]
            ],
        ]
    ];
}