<?php

use Bitrix\Main\Application;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Loader;
use Kodix\Payments\Tables\CurrencyTable;

define("ADMIN_MODULE_NAME", "kodix.payments");

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php');

Loc::loadLanguageFile(__FILE__);

$APPLICATION->SetTitle(GetMessage('KODIX_PAYMENTS_CURRENCY_LIST'));

if (!Loader::includeModule(ADMIN_MODULE_NAME)) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_after.php');
    ShowError(GetMessage('KODIX_PAYMENTS_ERR_NOT_INSTALLED'));
    require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/epilog_admin.php');
    die();
}

$MODULE_RIGHT = $APPLICATION->GetGroupRight(ADMIN_MODULE_NAME);

if ($MODULE_RIGHT == 'D') {
    $APPLICATION->AuthForm(GetMessage('ACCESS_DENIED'));
}

$action = strlen($_REQUEST['action']) ? $_REQUEST['action'] : strlen($_REQUEST['action_button']) ? $_REQUEST['action_button'] : '';
if ($MODULE_RIGHT == 'W' && $action == 'delete') {
    $db = Application::getConnection();
    $db->startTransaction();
    foreach ((array)$_REQUEST['ID'] as $id) {
        CurrencyTable::delete($id);
    }
    $db->commitTransaction();
}

$oSort = new CAdminSorting(CurrencyTable::getTableName(), 'ID', 'DESC');
$arOrder = [$oSort->getField() => $oSort->getOrder()];

$list = new CAdminList(CurrencyTable::getTableName(), $oSort);
$list->ShowChain($list->CreateChain());

$aContext = [
    [
        'TEXT' => GetMessage('KODIX_PAYMENTS_CURRENCY_NEW'),
        'TITLE' => GetMessage('KODIX_PAYMENTS_CURRENCY_NEW'),
        'ICON' => 'btn_new',
        'LINK' => 'kodix_payments_currency_edit.php?lang=' . LANGUAGE_ID
    ]
];
$list->AddAdminContextMenu($aContext);

$arHeaders = [];
$defaults = ['ID', 'NAME', 'NOMINAL', 'VALUE', 'CREATED_AT', 'UPDATED_AT'];
foreach (CurrencyTable::getMap() as $name => $field) {
    $title = GetMessage('KODIX_PAYMENTS_CURRENCY_' . $name);
    $arHeaders[] = [
        'id' => $name,
        'content' => strlen($title) ? $title : $field['title'],
        'sort' => $name,
        'default' => in_array($name, $defaults)
    ];
}
$list->AddHeaders($arHeaders);

$query = new CAdminResult(CurrencyTable::getList([
    'order' => $arOrder,
]), CurrencyTable::getTableName());
$query->NavStart();

$list->NavText($query->GetNavPrint(GetMessage('PAGES')));

while ($currency = $query->NavNext()) {
    $row = $list->AddRow($currency['ID'], $currency);

    $editLink = sprintf(
        'kodix_payments_currency_edit.php?ID=%s&lang=%s',
        $currency['ID'],
        LANGUAGE_ID
    );

    if ($MODULE_RIGHT == 'W') {
        $row->AddViewField(
            'NAME',
            sprintf(
                '<a href="%s">%s</a>',
                $editLink,
                htmlspecialchars($currency['NAME'])
            )
        );
        $row->AddActions([
            [
                'TEXT' => GetMessage('KODIX_PAYMENTS_CURRENCY_EDIT'),
                'ICON' => 'edit',
                'ACTION' => $list->ActionRedirect($editLink)
            ],
            [
                'TEXT' => GetMessage('KODIX_PAYMENTS_CURRENCY_REMOVE'),
                'ICON' => 'delete',
                'ACTION' => 'if(confirm("' . GetMessage('KODIX_PAYMENTS_CURRENCY_REMOVE_CONF') . '")) ' .
                    $list->ActionDoGroup($currency['ID'], 'delete')
            ]
        ]);
    }
}

if ($MODULE_RIGHT == 'W') {
    $list->AddGroupActionTable(
        [
            'delete' => true,
        ]
    );
}

$list->AddFooter([
    [
        'title' => GetMessage('MAIN_ADMIN_LIST_SELECTED'),
        'value' => 0
    ],
    [
        'counter' => true,
        'title' => GetMessage('MAIN_ADMIN_LIST_CHECKED'),
        'value' => 0
    ]
]);

$list->CheckListMode();

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_after.php');

$list->DisplayList();

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/epilog_admin.php');