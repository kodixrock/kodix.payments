<?php

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Loader;
use Kodix\Payments\Provider;
use Kodix\Payments\Tables\MerchantTable;

define("ADMIN_MODULE_NAME", "kodix.payments");

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");

Loc::loadLanguageFile(__FILE__);

$APPLICATION->SetTitle(GetMessage(strlen($_REQUEST['ID']) ? 'KODIX_PAYMENTS_MERCHANT_EDIT' : 'KODIX_PAYMENTS_MERCHANT_NEW'));
if (!Loader::includeModule(ADMIN_MODULE_NAME)) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_after.php');
    ShowError(GetMessage('KODIX_PAYMENTS_ERR_NOT_INSTALLED'));
    require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/epilog_admin.php');
    die();
}

$MODULE_RIGHT = $APPLICATION->GetGroupRight(ADMIN_MODULE_NAME);

if ($MODULE_RIGHT == 'D') {
    $APPLICATION->AuthForm(GetMessage('ACCESS_DENIED'));
}

$arErrors = [];
$arResult = [];
$merchant = false;

$providers = [];
foreach (Provider::getList() as $id => $provider) {
    $providers[$id] = $id;
}

if (strlen($_REQUEST['ID'])) {
    $merchant = MerchantTable::getById($_REQUEST['ID'])->fetch();
    if (is_array($merchant)) {
        $merchant['CONFIG'] = json_decode($merchant['CONFIG'], true);
        $arResult = $merchant;
    }
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $provider = Provider::get($_REQUEST['PROVIDER']);
    if (!is_null($provider)) {
        $props = $provider::getProperties();
        $id = $_REQUEST['PROVIDER'];
        $configs = [];
        foreach ($props as $key => $field) {
            $value = trim($_REQUEST['CONFIG'][$id][$key]);
            switch (true) {
                case $field['type'] == 'boolean':
                    $value = in_array($value, ['Y', 'y', 1, true]) ? 'Y' : 'N';
                    break;
                case !strlen($value) && $field['required']:
                    $title = GetMessage('KODIX_PAYMENTS_MERCHANT_PROPS_' . strtoupper($field['title']));
                    $arErrors[] = GetMessage(
                        'KODIX_PAYMENTS_ERR_PROVIDER_CONFIG',
                        ['#name#' => strlen($title) ? $title : $key]
                    );
                    break;
                case $field['type'] == 'secret':
                    if (!strlen(trim($value))) {
                        $value = $merchant['CONFIG'][$key];
                    }
                    break;
            }
            $configs[$key] = $value;
        }

        $arResult['CONFIG'] = $configs;
    } else {
        $arErrors[] = GetMessage('KODIX_PAYMENTS_ERR_PROVIDER');
    }

    if (empty($arErrors)) {
        $data = $arResult;
        foreach (MerchantTable::getMap() as $name => $field) {
            $value = isset($_REQUEST[$name]) ? $_REQUEST[$name] : $merchant[$name];
            switch (true) {
                case $name == 'CONFIG':
                    $value = json_encode($arResult['CONFIG']);
                    break;
                case $field['data_type'] == 'boolean':
                    $value = in_array($_REQUEST[$name], ['Y', 'y', 1, true]) ? 'Y' : 'N';
                    break;
            }
            if (!is_null($value)) {
                $data[$name] = $value;
            }
        }

        if (is_array($_FILES['IMAGE_ID'])) {
            $arImage = $_FILES['IMAGE_ID'];
            $arImage['MODULE_ID'] = ADMIN_MODULE_NAME;
            $fid = CFile::SaveFile($arImage, ADMIN_MODULE_NAME);
            if (intval($fid) > 0) {
                $data['IMAGE_ID'] = $fid;
            }
        }

        if (is_array($merchant)) {
            $result = MerchantTable::update($merchant['ID'], $data);
        } else {
            $result = MerchantTable::add($data);
        }

        if ($result->isSuccess()) {
            LocalRedirect(sprintf(
                'kodix_payments_merchant_edit.php?ID=%s&lang=%s',
                $result->getId(),
                LANGUAGE_ID
            ));
        } else {
            $arErrors = $result->getErrorMessages();
        }
    }
}

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

$arMenu = [
    [
        'TEXT' => GetMessage('KODIX_PAYMENTS_MERCHANT_LIST'),
        'LINK' => 'kodix_payments_merchant_list.php?lang=' . LANGUAGE_ID,
        'ICON' => 'btn_list',
        'TITLE' => '',
    ],
];
$context = new CAdminContextMenu($arMenu);
$context->Show();

if (!empty($arErrors)) {
    $message = new CAdminMessage(implode("<br>", $arErrors));
    echo $message->Show();
}

$arTabs = [
    [
        'DIV' => 'profile',
        'TAB' => GetMessage('KODIX_PAYMENTS_MERCHANT_PROFILE'),
        'TITLE' => GetMessage('KODIX_PAYMENTS_MERCHANT_PROFILE_TITLE'),
    ],
    [
        'DIV' => 'provider',
        'TAB' => GetMessage('KODIX_PAYMENTS_MERCHANT_PROVIDER'),
        'TITLE' => GetMessage('KODIX_PAYMENTS_MERCHANT_PROVIDER_TITLE'),
    ],
];
$form = new CAdminForm('folder_form', $arTabs);
$form->Begin(['FORM_ACTION' => sprintf(
    '%s?ID=%s&lang=%s',
    $APPLICATION->GetCurPage(),
    $arResult['ID'],
    LANGUAGE_ID
)]);
$form->BeginNextFormTab();
if (strlen($arResult['ID'])) {
    $form->AddViewField(
        'ID',
        GetMessage('KODIX_PAYMENTS_MERCHANT_ID'),
        $arResult['ID']
    );
    $form->AddViewField(
        'CREATED_AT',
        GetMessage('KODIX_PAYMENTS_MERCHANT_CREATED_AT'),
        $arResult['CREATED_AT']
    );
    $form->AddViewField(
        'UPDATED_AT',
        GetMessage('KODIX_PAYMENTS_MERCHANT_UPDATED_AT'),
        $arResult['UPDATED_AT']
    );
}
$form->AddEditField(
    'NAME',
    GetMessage('KODIX_PAYMENTS_MERCHANT_NAME'),
    true,
    ['size' => 40, 'maxlength' => 255],
    htmlspecialchars($arResult['NAME'])
);
$form->AddEditField(
    'SORT',
    GetMessage('KODIX_PAYMENTS_MERCHANT_SORT'),
    true,
    ['size' => 40, 'maxlength' => 255],
    htmlspecialchars($arResult['SORT'])
);
$form->AddCheckBoxField(
    'ACTIVE',
    GetMessage('KODIX_PAYMENTS_MERCHANT_ACTIVE'),
    true,
    'Y',
    $arResult['ACTIVE'] == 'Y'
);
$form->AddFileField(
    'IMAGE_ID',
    GetMessage('KODIX_PAYMENTS_MERCHANT_IMAGE'),
    $arResult['IMAGE_ID'],
    [],
    true
);

$form->BeginNextFormTab();
$form->AddDropDownField(
    'PROVIDER',
    GetMessage('KODIX_PAYMENTS_MERCHANT_PROVIDER'),
    true,
    $providers,
    $arResult['PROVIDER']
);
foreach ($providers as $id => $provider) {
    $instance = Provider::get($id);
    $props = $instance::getProperties();
    foreach ($props as $key => $field) {
        $name = sprintf('CONFIG[%s][%s]', $id, $key);
        $title = GetMessage('KODIX_PAYMENTS_MERCHANT_PROPS_' . strtoupper($field['title']));
        $value = $arResult['PROVIDER'] == $id ? $arResult['CONFIG'][$key] : null;

        switch ($field['type']) {
            case 'string':
                $form->AddEditField(
                    $name,
                    strlen($title) ? $title : $key,
                    $field['required'],
                    ['size' => 40, 'maxlength' => 255],
                    htmlspecialchars($value)
                );
                break;
            case 'secret':
                $form->AddEditField(
                    $name,
                    strlen($title) ? $title : $key,
                    $field['required'],
                    ['size' => 40, 'maxlength' => 255, 'type' => 'password']
                );
                break;
            case 'boolean':
                $form->AddCheckBoxField(
                    $name,
                    strlen($title) ? $title : $key,
                    $field['required'],
                    'Y',
                    $value
                );
                break;
        }
    }
}


$form->Buttons(['back_url' => 'kodix_payments_currency_list.php?lang=' . LANGUAGE_ID]);
$form->Show();

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");