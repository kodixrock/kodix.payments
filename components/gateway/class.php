<?php

use Bitrix\Main\Entity\FieldError;
use Bitrix\Main\Type\DateTime;
use Kodix\Payments\Controller;
use Kodix\Payments\Provider;
use Kodix\Payments\Exceptions\ModelException;
use Kodix\Payments\Models\AccountModel;
use Kodix\Payments\Models\MerchantModel;
use Kodix\Payments\Models\OrderModel;
use Kodix\Payments\Tables\OrderTable;

class Gateway extends Controller
{
    public function onPrepareComponentParams($arParams)
    {
        return ['FOLDER' => trim($arParams['SEF_FOLDER'])];
    }

    /**
     * @route /auth,post,rest
     */
    public function auth()
    {
        $token = null;
        $type = null;
        foreach (getallheaders() as $name => $value) {
            if (strtolower($name) == 'authorization') {
                list($type, $token) = explode(' ', $value, 2);
                $token = base64_decode($token);
                break;
            }
        }
        if (strtolower($type) != 'basic' || !is_string($token)) {
            $this->display(['message' => 'invalid token'], 401);
        }

        list($login, $secret) = explode(':', $token);
        $account = AccountModel::getByLogin($login, $secret);
        if (!$account) {
            $this->display(['message' => 'invalid credentials'], 401);
        }
        $account->logined = new DateTime();
        $account->save();
        return ['access_token' => $account->getToken()];
    }

    /**
     * @route /order,post,rest
     */
    public function newOrder()
    {
        $account = $this->getContext()->getAccount();
        if (is_null($account)) {
            $this->display(['message' => 'invalid credentials'], 403);
        }

        try {
            $order = OrderModel::json(file_get_contents('php://input'));
            $order->accountId = $account->id;
            $order->status = OrderTable::STATUS_NEW;
            $order->save();
        } catch (ModelException $e) {
            $error = array_shift($e->getErrors());
            $message = $error ? $error->getMessage() : $e->getMessage();
            if ($error instanceof FieldError) {
                $name = $error->getField()->getName();
                foreach ($order->getProperties() as $property) {
                    if (!empty($property['table']) && $property['table'][0] == $name && !empty($property['json'])) {
                        $orderModel = OrderModel::getTable();
                        $field = $orderModel::getMap()[$name];
                        $name = strlen($field['title']) ? $field['title'] : $property['table'][0];
                        $message = str_replace($name, $property['json'][0], $message);
                        break;
                    }
                }
            }
            $this->display(['message' => $message], 400);
        } catch (Exception $e) {
            $this->display(['message' => $e->getMessage()], 500);
        }

        $merchants = [];
        foreach (MerchantModel::getList(['filter' => ['ACTIVE' => 'Y']]) as $merchant) {
            $merchants[] = $merchant;
        }

        if (count($merchants) != 1) {
            $request = $this->getContext()->getRequest();
            $order->paymentUrl = sprintf(
                '%s://%s/%s/%s',
                $request->isHttps() ? 'https' : 'http',
                $request->getHttpHost(),
                trim($this->arParams['FOLDER'], DIRECTORY_SEPARATOR),
                $order->id
            );
        } else {
            $order->setMerchantId($merchants[0]->id);
            $order = $this->createOrder($order);
        }
        $order->save();

        return $order;
    }

    /**
     * @route /order/#id#,get,rest
     */
    public function getOrder($id)
    {
        $account = $this->getContext()->getAccount();
        if (is_null($account)) {
            $this->display(['message' => 'invalid credentials'], 403);
        }
        $order = OrderModel::getById($id);
        if (is_null($order)) {
            $this->display(['message' => 'order not found'], 404);
        }

        switch ($order->status) {
            case OrderTable::STATUS_CREATED:
                $merchant = $order->getMerchant();
                if (!is_null($merchant)) {
                    $provider = Provider::create($merchant->provider, $merchant->config);
                    if (!is_null($provider)) {
                        $order = $provider->updateOrder($order)->save();
                    }
                }
                break;
        }

        return $order;
    }

    /**
     * @route /#id#,get,post
     */
    public function payOrder($id)
    {
        $order = OrderModel::getById($id);
        if (!$order) {
            $this->notFound();
        }

        if ($order->status != OrderTable::STATUS_NEW) {
            header('Location: ' . $order->paymentUrl);
            die();
        }

        $this->getContext()->getRequest()->isPost() ? $this->orderAction($order) : $this->orderChallenge($order);
    }

    private function orderChallenge(OrderModel $order, $errors = [])
    {
        $this->arResult['errors'] = $errors;

        $this->arResult['order'] = !strlen($order->extId) ? $order->id : $order->extId;
        $this->arResult['amount'] = $order->amount;

        $currency = $order->getCurrency();
        if (!is_null($currency)) {
            $this->arResult['currency'] = !strlen($currency->shortName) ? $currency->name : $currency->shortName;
        }

        $merchants = MerchantModel::getList([
            'filter' => ['ACTIVE' => 'Y'],
            'order' => ['SORT' => 'ASC', 'NAME' => 'ASC'],
        ]);
        foreach ($merchants as $merchant) {
            $this->arResult['merchant'][$merchant->id] = [
                'name' => $merchant->name,
                'image' => $merchant->image,
            ];
        }

        $this->includeComponentTemplate();
    }

    private function orderAction(OrderModel $order)
    {
        try {
            $order->setMerchantId((int)$this->getContext()->getRequest()->getPost('merchant'));
            $order = $this->createOrder($order);
            $order->save();
        } catch (Exception $e) {
            $this->orderChallenge($order, [$e->getMessage()]);
            return;
        }
        header('Location: ' . $order->paymentUrl);
        die();
    }

    private function createOrder(OrderModel $order)
    {
        $merchant = $order->getMerchant();
        if (!is_null($merchant)) {
            $provider = Provider::create($merchant->provider, $merchant->config);
            if (!is_null($provider)) {
                return $provider->createOrder($order)->save();
            }
        }
        $order->status = OrderTable::STATUS_FAILED;
        $order->paymentUrl = strlen($order->failUrl) ? $order->failUrl : $order->successUrl;
        return $order;
    }
}

