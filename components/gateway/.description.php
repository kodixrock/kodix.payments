<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

$arComponentDescription = [
    'NAME' => GetMessage('KODIX_PAYMENTS_GATEWAY_NAME'),
    'DESCRIPTION' => GetMessage('KODIX_PAYMENTS_GATEWAY_DESCRIPTION'),
    'ICON' => '/images/sections_top_count.gif',
    'CACHE_PATH' => 'N',
    'SORT' => 100,
    'PATH' => [
        'ID' => 'Kodix.Payments',
        'NAME' => 'Kodix Online Payments',
    ]
];