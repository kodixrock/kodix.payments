<div class="vw_card">
    <?php if (count($arResult['errors']) > 0): ?>
        <ul class="vw_card__errors">
            <?php foreach ($arResult['errors'] as $error): ?>
                <li><?= $error ?></li>
            <?php endforeach; ?>
        </ul>
    <?php endif ?>
    <div class="vw_card__left">
        <div class="vw_card__left_inner">
            <div class="vw_card__header">
                <span><?= GetMessage('KODIX_PAYMENTS_GATEWAY_MERCHANT') ?></span>
            </div>
            <form method="post" class="choose-type">
                <?php foreach ($arResult['merchant'] as $id => $merchant): ?>
                    <button name="merchant" value="<?= $id ?>" class="choose-type__item">
                        <img src="<?= $merchant['image'] ?>" alt="<?= $merchant['name'] ?>">
                    </button>
                <?php endforeach;; ?>
            </form>
        </div>
    </div>
    <div class="vw_card__right">
        <div class="vw_card__right_inner">
            <div class="vw_card__order">
                <div class="vw_card__desc"><?= GetMessage('KODIX_PAYMENTS_GATEWAY_ORDER_NUMBER') ?></div>
                <div class="vw_card__main">
                    <span><?= $arResult['order'] ?></span>
                </div>
            </div>
            <div class="vw_card__price">
                <div class="vw_card__desc"><?= GetMessage('KODIX_PAYMENTS_GATEWAY_ORDER_AMOUNT') ?></div>
                <div class="vw_card__main">
                    <div class="price">
                        <span class="amount">
                            <?= number_format($arResult['amount'], 2, ',', ' ') ?>
                        </span>
                        <?= $arResult['currency'] ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>