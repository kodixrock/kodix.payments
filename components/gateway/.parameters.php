<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

$arComponentParameters = [
    'GROUPS' => [],
    'PARAMETERS' => [
        'SEF_FOLDER' => [
            'PARENT' => 'BASE',
            'NAME' => GetMessage('KODIX_PAYMENTS_GATEWAY_FOLDER'),
            'TYPE' => 'STRING',
            'REFRESH' => 'N',
        ],
    ],
];