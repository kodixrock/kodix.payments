#!/usr/bin/env bash

MODULE_ID='kodix.payments'
MODULE_NAMESPACE='Kodix\Payments'

baseDir="$(dirname "$0")/"
srcDir="${baseDir}lib"

target="${baseDir}include.php"

echo -e "<?php\n" > "$target"
echo "Bitrix\Main\Loader::registerAutoLoadClasses('${MODULE_ID}', [" >> "$target"

files=$(find "${srcDir}" -name "*.php")
for path in ${files[@]}; do
    name=${path:(${#srcDir}+1):(${#path}-${#srcDir}-5)}
    echo "    '${MODULE_NAMESPACE}\\${name/\//\\}' => 'lib/${name}.php'," >> "$target"
done
echo -e "]);\n" >> "$target"

echo "Kodix\Payments\Provider::register([" >> "$target"
providers=$(find "${srcDir}/Providers" -name "*.php")
for path in ${providers[@]}; do
    name=${path:(${#srcDir}+1):(${#path}-${#srcDir}-5)}
    echo "    ${MODULE_NAMESPACE}\\${name/\//\\}::class," >> "$target"
done
echo "]);" >> "$target"
